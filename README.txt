David Corrales Retana
B52232
Leonel Sánchez Lizano
B26213

Para ejecutarse se tiene que colocar en la carpeta 3_proj/build, presionar
make y luego:
gunzip -c ../mcf.trace.gz | ./src/l1cache -t 16 -l 32 -a 2 -cp l2

Parametros:

-t = tamaño de L1 en kB
-l = tamaño de la linea en bytes
-a = asociatividad
-cp = coherence protocol, la cual puede ser:
	msi o mesi.

Si se desea ejecutar los test se ejecuta el comando:
./test/cachetest

De igual manera para ejecutar los pthreads, se va a la carpeta 3_proj/build.
