#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <inttypes.h>
#include <debug_utilities.h>
using namespace std;

#define HIT_TIME 1
#define MISS_PENALTY 20

/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  printf("Do something :), don't forget to keep track of execution time");
  
  /*Initializations*********************************************/
  /*****************************************************************/
  /*****************************************************************/
  int cachesize_kb;
  int associativity;
  int blocksize_bytes;
  int status;
  int IC;
  int hm;
  int IC_count;
  
  int *tag_size = new int;
  int *idx_size = new int;
  int *offset_size = new int; 

  int *idx = new int;
  int *tag = new int;

  int dirty_evictions;
  int misses_Load;
  int misses_Store;
  int hits_Load;
  int hits_Store;
  int hits;
  int misses;
  
  int accesses;
  int AMAT;
  int CPU_time;

  double miss_rate;
  double hit_rate;
  double read_miss_rate;


  bool rp;          //LRU: rp=true; SSRIP: rp=false
  bool *rpp = &rp;
  bool loadstore;
  bool debug = false;

  long address;

  char num;
  char tracedata[8];

  string argvi;
  string argvi_plus1;  

  operation_result op_result;
  operation_result *p_op_result = &op_result;

  /*****************************************************************/
  /*****************************************************************/
  /*****************************************************************/
  /* Parse argruments */
  /* Meter en Función!!! */
  for (int i = 1; i < argc; i++){
    argvi = argv[i];
    argvi_plus1 = argv[i+1];

    if (argvi== "-t"){ cachesize_kb = atoi(argv[i+1]); i++;
      cout << cachesize_kb << endl;
    }
    else
    if (argvi== "-l"){ blocksize_bytes = atoi(argv[i+1]);i++;
      cout << blocksize_bytes << endl;
    }
    else
    if (argvi== "-a"){ associativity = atoi(argv[i+1]);i++;
      cout << associativity << endl;
    }
    else 
    if (argvi == "-rp"){ 
      if(argvi_plus1 == "lru"){
        rp = true;
        i++;
          cout << rp << endl;

      }else
      if (argvi_plus1=="srrip"){
        rp = false;
        i++;
          cout << rp << endl;
      }      
    } 
  }
  

  
  /*****************************************************************/
  /*Cache creation*/
  status = field_size_get(
    cachesize_kb,
    associativity,
    blocksize_bytes,
    tag_size,
    idx_size,
    offset_size
  );


  if(status == ERROR){ cout << "field_size_get = ERROR" << endl; exit(0);}

  entry **cache = create_cache(associativity, *idx_size);
  



  /* Get trace's lines and start your simulation */
  bool read = true;
  IC_count = 0;

  /*Initialize Dirty Evictions*/
   dirty_evictions = 0;
  /*Initialize hitmiss Totals*/
   misses_Load = 0;
   misses_Store = 0;
   hits_Load = 0;
   hits_Store = 0;


   string hash = "#";
   int c = 0;

  while(hash == "#"){
        
        c++;
        /*Read Trace*/
        /*Check for # */
        hash = "not hash lol";

        cin >> hash;

        /*if(c==20000){
         hash = "im out";
        }*/

          /*Read LoadStore*/
          cin >> tracedata;
          loadstore = atoi(tracedata);
          cout << "loadstore: " << loadstore << endl;

          /*Read Address*//*
          cin >> tracedata;
          address = atol(tracedata);
          cout << "address:  " << address << endl;*/
          cin >> hex >> address;
          cout << "address:  " << address << endl;

          /*Read IC*/
          cin >> tracedata;
          IC = atoi(tracedata);
          IC_count = IC_count + IC;
          cout << "IC:   "<< IC << endl;

          /*Process Trace Info*/
          /*Get Tag & Index*/
          address_tag_idx_get(address,
                          *tag_size,
                          *idx_size,
                          *offset_size,
                          idx,
                          tag);

          /*Apply Replacement Algorithm*/

          if(rp == true){
            status = lru_replacement_policy(
              *idx,
              *tag,
              associativity,
              loadstore,
              cache[*idx], 
              p_op_result,            
              debug);

            if(status == ERROR){ cout << "lru_replacement_policy = ERROR" << endl; exit(0);}
          }else{
            status = srrip_replacement_policy(
              *idx,
              *tag,
              associativity,
              loadstore,
              cache[*idx],
              p_op_result,            
              debug);
              if(status == ERROR){ cout << "srrip_replacement_policy = ERROR" << endl; exit(0);}
          }

        /*Post Result Operations*/
        /*Dirty Evictions*/
        if(op_result.dirty_eviction == true){
          dirty_evictions++;
        }

        /*Updates Hit-Miss counters*/
        /*Hacer Función!!!!*/
        switch (op_result.miss_hit){
          case MISS_LOAD:
            misses_Load++;
            break;

          case MISS_STORE:
            misses_Store++;
            break;

          case HIT_LOAD:
            hits_Load++;
            break;

          case HIT_STORE:
            hits_Store++;
            break;
          
          default:
            break;
        }
    }

  /*Final Hit-Miss count*/
  hits = hits_Load + hits_Store;
  misses = misses_Load + misses_Store;
  accesses = hits + misses;

  /*Hit/Miss Rate*/
  miss_rate = double(misses) / double(accesses);
  hit_rate = double(hits) / double(accesses);
  read_miss_rate = double(misses_Load) / double(misses);

  /*CPU Time*/
  CPU_time = IC_count + (HIT_TIME + MISS_PENALTY)*misses_Load;

  /*AMAT*/
  AMAT = HIT_TIME + miss_rate*MISS_PENALTY;


  /*********************************************************************/
  /* Print cache configuration */
  /* HACER FUNCION QUE IMPRIMA TODO*/

  cout << endl;
  cout << "*****************************************" << endl;
  cout << "Cache Parameters" << endl;
  cout << "*****************************************" << endl;
  cout << "Cache Size (KB): " << ".........." << cachesize_kb << endl;
  cout << "Cache Associativity: " << "......" << associativity << endl;
  cout << "Cache Block Size (bytes):" << ".." << blocksize_bytes << endl;
  

    /* Print Statistics */

  cout << "*****************************************" << endl;
  cout << "Simulation results:" << endl;
  cout << "*****************************************" << endl;
  cout << "CPU time (cycles):" << "........." << CPU_time << endl;
  cout << "AMAT(cycles):" << ".............." << AMAT << endl;
  cout << "Overall: miss rate" << "........." << miss_rate << endl;
  cout << "Read miss rate:" << "............" << read_miss_rate << endl;
  cout << "Dirty evictions:" << "..........." << dirty_evictions << endl;
  cout << "Load misses:" << "..............." << misses_Load << endl;
  cout << "Store misses:" << ".............." << misses_Store << endl;
  cout << "Total misses:" << ".............." << misses << endl;
  cout << "Load hits:" << "................." << hits_Load << endl;
  cout << "Store hits:" << "................" << hits_Store << endl;
  cout << "Total hits:" << "................" << hits << endl;
  cout << "Total memory accesses:" << "....." << accesses << endl;
  cout << "*****************************************" << endl;
 

  // Prints de prueba
  cout << "_____________________________" << '\n';
  cout << tag_size << '\n';
  cout << idx_size << '\n';
  cout << offset_size << '\n';


/*cout << "------------print---idx:"<< *idx_size<<endl;
  for (int i = 0; i < pow(2,*idx_size); i++){
    for (int j = 0; j < associativity; j++){
      printf("%" PRIu8 , cache[i][j].rp_value);
      //cout << " (";
      //printf("%" PRIu8 , cache[i][j].valid);
      cout << ",  ";
    }
    cout << endl;
    cout << endl;

    }*/
   
return 0;
}































 /*printf("%#010x\n", maskTag);
 
   cout << "_____________________________" << '\n';*/

   /*********************test  a create matrix
    * 
    * 
    *   int x = 10;
  int y = 10;
  entry **cache = create_cache(x, y);
  cout << "------------print"<< endl;
  for (int i = 0; i < x; i++){
    for (int j = 0; j < y; j++){
      printf("%" PRIu8 , cache[i][j].rp_value);
      cout << ",  ";
    }
    cout << endl;
    }

    ********************************************///**/

    /*




  cout << "------------print---idx:"<< *idx_size<<endl;
  for (int i = 0; i < pow(2,*idx_size); i++){
    for (int j = 0; j < associativity; j++){
      printf("%" PRIu8 , cache[i][j].rp_value);
      cout << ",  ";
    }
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    }

    exit(0);*/


  //long address = 0xDEADCAFE;


   /*address_tag_idx_get(address,
                         tag_size,
                         idx_size,
                         offset_size,
                         idxp,
                         tagp
   );*/
