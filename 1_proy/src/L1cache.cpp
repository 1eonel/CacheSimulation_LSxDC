/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

/*************************************************************/
int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{

   //Add return ERROR when needed.
   bool cachesize_check = pow_of_2(cachesize_kb);
   bool associativity_check = pow_of_2(associativity);
   bool blocksize_check = pow_of_2(blocksize_bytes);

   if (cachesize_check == false ||
      associativity_check == false ||
      blocksize_check == false){
         return ERROR;
   }

   /*Offset*/
   *offset_size = log2(blocksize_bytes);

   /*Index for different Associativities*/
   if (associativity > 0){
      *idx_size = log2((cachesize_kb * KB) / (blocksize_bytes * associativity));
   }
   else{
      *idx_size = log2((cachesize_kb * KB) / blocksize_bytes);
   }

   /*Tag*/
   *tag_size = ADDRSIZE - *idx_size - *offset_size;

  return OK;
}
/*************************************************************/
void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{
   /*Tag*/
   *tag = address >> (ADDRSIZE - tag_size);

   /*Index*/
   int maskIdx = pow(2, idx_size) - 1;
   *idx = address >> offset_size;
   *idx = *idx & maskIdx;

}
/*************************************************************/
int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{


  /**********Static - Re- Reference Interval Prediction******/
  /**********Least Recently Used = 0 ************/


  // Cache Hit:
  // (i) set RRPV of block to ‘0’ (Rereference Prediction Values)

  // Cache Miss:
  // (i) search for first ‘3’ from left
  // (ii) if ‘3’ found go to step (v)
  // (iii) increment all RRPVs (Rereference Prediction Values)
  // (iv) goto step (i)
  // (v) replace block and set RRPV to ‘2’


  /*Flag for detecting a Hit. Helps with function flow.*/
 bool hit = false;


 /***HIT***/
 /*Search for Tag in all the sets*/

 for (int a = 0; a < associativity; a++){
    if (cache_blocks[a].valid && tag == cache_blocks[a].tag){
       /*Found matching Tag with valid up.*/
       cout<<"HIT"<<endl;
       hit = true;

       /*****Write Result****/
       /*Hit Load or Store?*/
       if (loadstore == true){ //Load
          result->miss_hit = HIT_LOAD;
          cache_blocks[a].dirty = false;
       }else {                 //Store
          result->miss_hit = HIT_STORE;
          cache_blocks[a].dirty = true;
       }

       /*Dirty Eviction*/
       result->dirty_eviction = false;

       /*No Evicted Address when Hit*/
       result->evicted_address = 0;

       cache_blocks[a].rp_value = 0;

       /*Loop ends with hit*/
       break;
    }
 }

 /***MISS***/
 if (hit == false){
    cout <<endl;
    cout<<"MISS"<<endl;

    for (int a = 0; a < associativity; a++){
       /*Search for LRU Way*/

       if (cache_blocks[a].rp_value == 3){

          /*****Write Result****/
          /*Dirty Eviction?*/
          if (cache_blocks[a].dirty == true){ //block is dirty
             result->dirty_eviction = true;
          }else{                              //block is not dirty
             result->dirty_eviction = false;
          }

          /*Miss Load or Store?*/
          if (loadstore == true){ //Load
             result->miss_hit = MISS_LOAD;
             cache_blocks[a].dirty = false;
          }else {                 //Store
             result->miss_hit = MISS_STORE;
             cache_blocks[a].dirty = true;
          }

          /*Evicted Address*/
          result->evicted_address = cache_blocks[a].tag;

       /*******Update Replacement Policy Values******/
       /*1 Case:
          A-> rp_value[ref block] == 0  {--All}*/



       /*Execute block replacement*/
       cache_blocks[a].valid = true;
       cache_blocks[a].tag = tag;
       cache_blocks[a].rp_value = 2;
       /*Loop ends with miss*/
       break;

       }

       else{
         cache_blocks[a].rp_value ++;

       }
    }
 }



   return OK;
}

/*************************************************************/
int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
    /**********Most Recently Used = Assoc - 1******/
    /**********Least Recently Used = 0 ************/


    /*Flag for detencting a Hit. Helps with function flow.*/
   bool hit = false;


   /***HIT***/
   /*Search for Tag in the all the sets*/

   for (int a = 0; a < associativity; a++){
      if (cache_blocks[a].valid && tag == cache_blocks[a].tag){
         /*Found matching Tag with valid up.*/
         cout<<"HIT"<<endl;
         hit = true;

         /*****Write Result****/
         /*Hit Load or Store?*/
         if (loadstore == true){ //Load
            result->miss_hit = HIT_LOAD;
            cache_blocks[a].dirty = false;
         }else {                 //Store
            result->miss_hit = HIT_STORE;
            cache_blocks[a].dirty = true;
         }

         /*Dirty Eviction*/
         result->dirty_eviction = false;

         /*No Evicted Address when Hit*/
         result->evicted_address = 0;

         /*******Update Replacement Policy Values******/
         /*3 Cases:
            A-> rp_value[ref block] == MRU      {No rp_value decrement}
            B-> rp_value[ref block] == 0        {--All}
            C-> 0 < rp_value[ref block] < MAX   {-- only > rp_value[referenced block]} */

         //for (int i = 0; i < associativity; i++){

            if (cache_blocks[a].rp_value == 0)                           //B
               {for (int i = 0; i < associativity; i++){
                     if (cache_blocks[i].rp_value != 0){
                           cache_blocks[i].rp_value--;}
               }

            }else if(cache_blocks[a].rp_value < associativity -1){

               for (int i = 0; i < associativity; i++){                                                 //C
                  if (cache_blocks[i].rp_value > cache_blocks[a].rp_value){     //C

                     cache_blocks[i].rp_value--;
                  }
               }
            }
            cache_blocks[a].rp_value = associativity - 1; // Update MRU

         /*Loop ends with hit*/
         break;
      }
   }

   /***MISS***/
   if (hit == false){
      cout <<endl;
      cout<<"MISS"<<endl;

      for (int a = 0; a < associativity; a++){
         /*Search for LRU Way*/

         if (cache_blocks[a].rp_value == 0){
            cout << "LRU WAY:  " << a <<endl;
            /*****Write Result****/
            /*Dirty Eviction?*/
            if (cache_blocks[a].dirty == true){ //block is dirty
               result->dirty_eviction = true;
            }else{                              //block is not dirty
               result->dirty_eviction = false;
            }

            /*Miss Load or Store?*/
            if (loadstore == true){ //Load
               result->miss_hit = MISS_LOAD;
               cache_blocks[a].dirty = false;
            }else {                 //Store
               result->miss_hit = MISS_STORE;
               cache_blocks[a].dirty = true;
            }

            /*Evicted Address*/
            result->evicted_address = cache_blocks[a].tag;

         /*******Update Replacement Policy Values******/
         /*1 Case:
            A-> rp_value[ref block] == 0  {--All}*/

         for (int i = 0; i < associativity; i++){

            if (cache_blocks[i].rp_value != 0){
               cache_blocks[i].rp_value--;
            }
         }
         cache_blocks[a].rp_value = associativity - 1; // Update MRU

         /*Execute block replacement*/
         cache_blocks[a].valid = true;
         cache_blocks[a].tag = tag;

         /*Loop ends with miss*/
         break;

         }
      }
   }
   return OK;
}
/*************************************************************/

entry **create_cache (int associativity,
                     int idx_size)
{
   int ways = associativity;
   int sets = pow(2, idx_size);

   /*Cache Matrix creation*/
   entry **cache = new entry*[sets];
   for(int w = 0; w < sets; w++){
      cache[w] = new entry[ways];
   }

   /*Cache initialization*/

   /*LRU counters are initialized the following way*/
   /* Way0 = 0 ; Way1 = 1 ; Way2 = 2; ... ; WayN = N*/
   for (int i = 0; i < sets; i++)
   {

      for (int j = 0; j < ways; j++)
      {

         cache[i][j].valid = 0;
         cache[i][j].dirty = 0;
         cache[i][j].tag = 0;
         cache[i][j].rp_value = j;
         //cache[i][j].rp_value = 3;



      }
   }
   return cache;
}


/*Helper Functions*/

bool pow_of_2(int v){

   bool isPow = (v & (v - 1)) == 0;

   //f = v && !(v & (v - 1)); 0 = false
   return isPow;
}
