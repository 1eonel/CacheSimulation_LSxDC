/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#ifndef L1CACHE_H
#define L1CACHE_H

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
using namespace std;

/*
 * ENUMERATIONS
 */

/* Return Values */
enum returns_types {
  OK,
  PARAM,
  ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
  LRU,
  NRU,
  RRIP,
  RANDOM
};

enum miss_hit_status {
  MISS_LOAD,
  MISS_STORE,
  HIT_LOAD,
  HIT_STORE
};

enum MSI_MESI_status {
  MODIFIED,
  EXCLUSIVE,
  SHARED,
  INVALID
};

/*
 * STRUCTS
 */

/* Cache tag array fields */
struct entry {
  bool valid ;
  bool dirty;
  int tag ;
  uint8_t rp_value ;
  enum MSI_MESI_status coherence_protocol;
};

/* Cache replacement policy results */
struct operation_result {
  enum miss_hit_status miss_hit;
  bool dirty_eviction;
  int  evicted_address;
};

/*
 *  Functions
 */

 //creates cache matrix variable
 entry** cache_build( int idx_size,
                      int associativity,
                      string rp);






/*
 * Get tag, index and offset length
 *
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] tag_size: size in bits of the tag field
 * [out] idx_size: size in bits of the index field
 * [out] offset_size: size in bits of the offset size
 */
int field_size_get_cache_build(int cachesize_kb,
                   int associativity,
                   int blocksize_bytes,
                   int *tag_size,
                   int *idx_size,
                   int *offset_size,
                   //entry ***l1_cache,
                   string rp);

/*
 * Get tag and index from address
 *
 * [in] address: memory address
 * [in] tag_size: number of bits of the tag field
 * [in] idx_size: number of bits of the index field
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */

void address_tag_idx_get(long address,
                         int tag_size,
                         int idx_size,
                         int offset_size,
                         int *idx,
                         int *tag);


/*
 * Search for an address in a cache set and
 * replaces blocks using LRU policy
 *
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation true if load false if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_msi_mesi_prot (int number_of_entries,
                           int tag,
                           int associativity,
                           bool loadstore,
                           bool msi_mesi,
                           entry* cache_blocks,
                           entry* cache_blocks_CPU2,
                           operation_result* operation_result,
                           int* LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                           int* LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                           bool debug=false);


int lru_L2_cache (int number_of_entries,
                  int tag,
                  int associativity,
                  bool loadstore,
                  entry* cache_blocks,
                  operation_result* operation_result,
                  int* LS_miss_hit_eviction_count,
                  bool debug=false);


/*
* Search for an address in a L1 cache set and, if miss, search in L2
* if HIT in L2, take that address block to L1 and the replaced block in L1 is discarded
* If MISS in L2, replaces the block in L1 and the replaced block is discarded,
* then replaces the block in L2 and goes to memory with writeback, check dirty bit status.
* Replaces blocks using LRU policy,
* (as it is writethrough, L2 has all L1 blocks).
*/
int lru_multilevel_cache (int idx_L1,
                          int idx_L2,
                          int tag_L1,
                          int tag_L2,

                          int associativity_L1,
                          int associativity_L2,

                          bool loadstore,
                          bool msi_mesi,

                          entry* cache_L1_CPU1_blocks,
                          entry* cache_L1_CPU2_blocks,
                          entry* cache_L2_blocks,

                          operation_result* operation_result_for_L1,
                          operation_result* operation_result_for_L2,

                          int* LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                          int* LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                          int* LS_miss_hit_eviction_count_for_L2,
                          bool debug = false);








#endif
