/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#define YEL   "\x1B[33m"

/* Globals */
int debug_on = 1;

/* Test Helpers */
#define DEBUG(x) if (debug_on) printf("%s\n",#x)

/*
-----------------------------------------------MULTILEVEL CACHE WITH COHERENCE PROTOCOL-----------------------------------------


Test1: CPU1 read + CPU2 write
Stimuli
1. Choose a random associativity
2. Choose a random address (AddressA)
3. Fill a l1 CPU 1 cache line with random addresses, making sure AddressA is not added.
4. Fill a l1 CPU 2 cache line with shared addresses, making sure AddressA is not added.
5. Read Address A in CPU 1. Check 1,2.
6. Write Address A in CPU 1. Check 3,4.

Checks
1. Check coherence protocol status in CPU 1 changed to EXCLUSIVE.
2. Check coherence protocol status in CPU 2 is still SHARED.
3. Check coherence protocol status in CPU 1 changed from EXCLUSIVE to MODIFIED.
4. Check coherence protocol status in CPU 2 changed from SHARED to INVALID.
*/
TEST(Multilevel_cache_cohe_prot, MESI){
  int status;
  int i;
  int j;

  int idx_L1;
  int idx_L2;
  int tag_L1;
  int tag_L2;
  int tag_size_L1;
  int tag_size_L2;
  int idx_size_L1;
  int idx_size_L2;
  int offset_size;
  int associativity_L1;
  int associativity_L2;
  int* L1_stats_count_CPU1 = new int[6];
  int* L1_stats_count_CPU2 = new int[6];
  int* L2_stats_count = new int[5];

  //initialize counters:
  L1_stats_count_CPU1[0] = 0;
  L1_stats_count_CPU1[1] = 0;
  L1_stats_count_CPU1[2] = 0;
  L1_stats_count_CPU1[3] = 0;
  L1_stats_count_CPU1[4] = 0;
  L1_stats_count_CPU1[5] = 0;
  //initialize counters:
  L1_stats_count_CPU2[0] = 0;
  L1_stats_count_CPU2[1] = 0;
  L1_stats_count_CPU2[2] = 0;
  L1_stats_count_CPU2[3] = 0;
  L1_stats_count_CPU2[4] = 0;
  L1_stats_count_CPU2[5] = 0;
  //initialize counters:
  L2_stats_count[0] = 0;
  L2_stats_count[1] = 0;
  L2_stats_count[2] = 0;
  L2_stats_count[3] = 0;
  L2_stats_count[4] = 0;


  enum MSI_MESI_status expected_cohe_prot_for_CPU1;
  enum MSI_MESI_status expected_cohe_prot_for_CPU2;

  //EXCLUSIVE test
  int cohe_test_CPU1_E;
  int cohe_test_CPU2_E;
  //MODIFIED test
  int cohe_test_CPU1_M;
  int cohe_test_CPU2_M;

  bool loadstore = 1;
  bool debug = 0;
  bool extra_debug = 0;
  struct operation_result result_for_L1_CPU1 = {};
  struct operation_result result_for_L1_CPU2 = {};
  struct operation_result result_for_L2 = {};

  //Coherence Protocol
  bool msi_mesi = true;

  /* Fill a random cache entry */
  //offset same size on both caches
  offset_size = log2(1024); // 10 bits
  //for L1:
  idx_L1 = rand()%1024;
  tag_L1 = rand()%4096;
  associativity_L1 = 2 << (rand()%4);
  /*
  *offset_size = log2(blocksize_bytes);
  *idx_size = log2(cache_true_size / (blocksize_bytes * ways));
  *tag_size = ADDRSIZE - *idx_size - *offset_size;
  */
  idx_size_L1 = log2(1024); // = 10 bits
  tag_size_L1 = log2(4096); // = 12 bits
  //address, with offset size = 10, would be:
  //address_size = 32 bits;
  long address_size = tag_size_L1 + idx_size_L1 + offset_size;
  //cout << "\n\n ADDRESS size: " << address_size;

  //for L2:
  //L2 has two times L1's associativity
  associativity_L2 = 2*associativity_L1;

  //4 times its cache size means 2 more bits for the idx
  idx_size_L2 = idx_size_L1 + 2; // 4 bits
  //we take this tag size based on L1;
  //so, address_size - idx_size_L2 - offset_size = tag_size_L2
  tag_size_L2 = address_size - idx_size_L2 - offset_size; // 18 bits
  if (debug) {
    cout << "\n\n tag_sizeL2: " << tag_size_L2 << "\n\n";
    cout << "\n\n tag L1: " << tag_L1 << endl;
    cout << " idx L1: " << idx_L1 << "\n\n";
  }

  struct entry cache_line_L1_CPU1[associativity_L1];
  struct entry cache_line_L1_CPU2[associativity_L1];
  struct entry cache_line_L2[associativity_L2];

  int rand_tag_for_L2 =  pow(2,tag_size_L2);

  DEBUG(Checking CPU coherence protocol changes mesi);
  long address_got_from_L1_i = 0;
  int tag_L1_i;

  //starts test for
  for (i = 0 ; i < 1; i++){

    /* Fill cache line */
    for ( i =  0; i < associativity_L1; i++) {
      //-------------------------------------------------------------------------------------------------
      tag_L1_i = rand()%4096;

      //CPU1
      cache_line_L1_CPU1[i].valid = true;
      cache_line_L1_CPU1[i].tag = tag_L1_i;
      cache_line_L1_CPU1[i].dirty = 0;
      cache_line_L1_CPU1[i].rp_value = i;
      cache_line_L1_CPU1[i].coherence_protocol = SHARED;
      //CPU2
      cache_line_L1_CPU2[i].valid = true;
      cache_line_L1_CPU2[i].tag = tag_L1_i;
      cache_line_L1_CPU2[i].dirty = 0;
      cache_line_L1_CPU2[i].rp_value = i;
      cache_line_L1_CPU2[i].coherence_protocol = SHARED;
      while (cache_line_L1_CPU1[i].tag == tag_L1) {
        cache_line_L1_CPU1[i].tag = rand()%4096;
        cache_line_L1_CPU2[i].tag = cache_line_L1_CPU1[i].tag;
      }
    }
    //-------------------------------------------------------------------------------------------------


    // EXCLUSIVE TEST
    loadstore = false; //its a read

    status = lru_multilevel_cache (idx_L1,
                              idx_L2,
                              tag_L1,
                              tag_L2,

                              associativity_L1,
                              associativity_L2,

                              loadstore,
                              msi_mesi,

                              cache_line_L1_CPU1,
                              cache_line_L1_CPU2,
                              cache_line_L2,

                              &result_for_L1_CPU1,
                              &result_for_L2,

                              L1_stats_count_CPU1,
                              L1_stats_count_CPU2,
                              L2_stats_count,
                              debug
    );

    for ( i =  0; i < associativity_L1; i++) {
      if (cache_line_L1_CPU1[i].tag == tag_L1) {
        cohe_test_CPU1_E = cache_line_L1_CPU1[i].coherence_protocol;
        cohe_test_CPU2_E = cache_line_L1_CPU2[i].coherence_protocol;
      }
    }

    //MODIFIED test 1
    loadstore = true;
    status = lru_multilevel_cache (idx_L1,
                              idx_L2,
                              tag_L1,
                              tag_L2,

                              associativity_L1,
                              associativity_L2,

                              loadstore,
                              msi_mesi,

                              cache_line_L1_CPU1,
                              cache_line_L1_CPU2,
                              cache_line_L2,

                              &result_for_L1_CPU1,
                              &result_for_L2,

                              L1_stats_count_CPU1,
                              L1_stats_count_CPU2,
                              L2_stats_count,
                              debug
    );

    for ( i =  0; i < associativity_L1; i++) {
      if (cache_line_L1_CPU1[i].tag == tag_L1) {
        cohe_test_CPU1_M = cache_line_L1_CPU1[i].coherence_protocol;
        cohe_test_CPU2_M = cache_line_L1_CPU2[i].coherence_protocol;
      }
    }

    /*
    Test1: CPU1 read + CPU2 write
    Stimuli
    1. Choose a random associativity
    2. Choose a random address (AddressA)
    3. Fill a l1 CPU 1 cache line with random addresses, making sure AddressA is not added.
    4. Fill a l1 CPU 2 cache line with shared addresses, making sure AddressA is not added.
    5. Read Address A in CPU 1. Check 1,2.
    6. Write Address A in CPU 1. Check 3,4.

    Checks
    1. Check coherence protocol status in CPU 1 changed to EXCLUSIVE.
    2. Check coherence protocol status in CPU 2 is still SHARED.
    3. Check coherence protocol status in CPU 1 changed from EXCLUSIVE to MODIFIED.
    4. Check coherence protocol status in CPU 2 changed from SHARED to INVALID.
    */

    EXPECT_EQ(status, 0);
    expected_cohe_prot_for_CPU1 = EXCLUSIVE;
    expected_cohe_prot_for_CPU2 = SHARED;
    EXPECT_EQ(cohe_test_CPU1_E, expected_cohe_prot_for_CPU1);
    EXPECT_EQ(cohe_test_CPU2_E, expected_cohe_prot_for_CPU2);

    expected_cohe_prot_for_CPU1 = MODIFIED;
    expected_cohe_prot_for_CPU2 = INVALID;
    EXPECT_EQ(cohe_test_CPU1_M, expected_cohe_prot_for_CPU1);
    EXPECT_EQ(cohe_test_CPU2_M, expected_cohe_prot_for_CPU2);

  }  //ends test for
}// end Test1



/*
Test2: CPU1 write + CPU2 read
Stimuli
1. Choose a random associativity
2. Choose a random address (AddressA)
3. Fill a l1 CPU 1 cache line with random addresses, making sure AddressA is not added.
4. Fill a l1 CPU 2 cache line with shared addresses, making sure AddressA is not added.
6. Write Address A in CPU 2. Check 1,2.
5. Read Address A in CPU 1. Check 3,4.

Checks
1. Check coherence protocol status in CPU 2 changed to MODIFIED.
2. Check coherence protocol status in CPU 1 changed from SHARED to INVALID.
3. Check coherence protocol status in CPU 2 changed from MODIFIED to SHARED.
4. Check coherence protocol status in CPU 1 changed from INVALID to SHARED.
*/
TEST(Multilevel_cache_cohe_prot, MSI){
  int status;
  int i;
  int j;

  int idx_L1;
  int idx_L2;
  int tag_L1;
  int tag_L2;
  int tag_size_L1;
  int tag_size_L2;
  int idx_size_L1;
  int idx_size_L2;
  int offset_size;
  int associativity_L1;
  int associativity_L2;
  int* L1_stats_count_CPU1 = new int[6];
  int* L1_stats_count_CPU2 = new int[6];
  int* L2_stats_count = new int[5];

  //initialize counters:
  L1_stats_count_CPU1[0] = 0;
  L1_stats_count_CPU1[1] = 0;
  L1_stats_count_CPU1[2] = 0;
  L1_stats_count_CPU1[3] = 0;
  L1_stats_count_CPU1[4] = 0;
  L1_stats_count_CPU1[5] = 0;
  //initialize counters:
  L1_stats_count_CPU2[0] = 0;
  L1_stats_count_CPU2[1] = 0;
  L1_stats_count_CPU2[2] = 0;
  L1_stats_count_CPU2[3] = 0;
  L1_stats_count_CPU2[4] = 0;
  L1_stats_count_CPU2[5] = 0;
  //initialize counters:
  L2_stats_count[0] = 0;
  L2_stats_count[1] = 0;
  L2_stats_count[2] = 0;
  L2_stats_count[3] = 0;
  L2_stats_count[4] = 0;


  enum MSI_MESI_status expected_cohe_prot_for_CPU1;
  enum MSI_MESI_status expected_cohe_prot_for_CPU2;

  //MODIFIED test
  int cohe_test_CPU1_M_2;
  int cohe_test_CPU2_M_2;
  //SHARED test
  int cohe_test_CPU1_S;
  int cohe_test_CPU2_S;


  bool loadstore = 1;
  bool debug = 0;
  bool extra_debug = 0;
  struct operation_result result_for_L1_CPU1 = {};
  struct operation_result result_for_L1_CPU2 = {};
  struct operation_result result_for_L2 = {};

  //Coherence Protocol
  bool msi_mesi = false;

  /* Fill a random cache entry */
  //offset same size on both caches
  offset_size = log2(1024); // 10 bits
  //for L1:
  idx_L1 = rand()%1024;
  tag_L1 = rand()%4096;
  associativity_L1 = 2 << (rand()%4);
  /*
  *offset_size = log2(blocksize_bytes);
  *idx_size = log2(cache_true_size / (blocksize_bytes * ways));
  *tag_size = ADDRSIZE - *idx_size - *offset_size;
  */
  idx_size_L1 = log2(1024); // = 10 bits
  tag_size_L1 = log2(4096); // = 12 bits
  //address, with offset size = 10, would be:
  //address_size = 32 bits;
  long address_size = tag_size_L1 + idx_size_L1 + offset_size;
  //cout << "\n\n ADDRESS size: " << address_size;

  //for L2:
  //L2 has two times L1's associativity
  associativity_L2 = 2*associativity_L1;

  //4 times its cache size means 2 more bits for the idx
  idx_size_L2 = idx_size_L1 + 2; // 4 bits
  //we take this tag size based on L1;
  //so, address_size - idx_size_L2 - offset_size = tag_size_L2
  tag_size_L2 = address_size - idx_size_L2 - offset_size; // 18 bits
  if (debug) {
    cout << "\n\n tag_sizeL2: " << tag_size_L2 << "\n\n";
    cout << "\n\n tag L1: " << tag_L1 << endl;
    cout << " idx L1: " << idx_L1 << "\n\n";
  }

  struct entry cache_line_L1_CPU1[associativity_L1];
  struct entry cache_line_L1_CPU2[associativity_L1];
  struct entry cache_line_L2[associativity_L2];

  int rand_tag_for_L2 =  pow(2,tag_size_L2);

  DEBUG(Checking CPU coherence protocol changes msi);
  long address_got_from_L1_i = 0;
  int tag_L1_i;

  //starts test for
  for (i = 0 ; i < 1; i++){

    /* Fill cache line */
    for ( i =  0; i < associativity_L1; i++) {
      //-------------------------------------------------------------------------------------------------
      tag_L1_i = rand()%4096;

      //CPU1
      cache_line_L1_CPU1[i].valid = true;
      cache_line_L1_CPU1[i].tag = tag_L1_i;
      cache_line_L1_CPU1[i].dirty = 0;
      cache_line_L1_CPU1[i].rp_value = i;
      cache_line_L1_CPU1[i].coherence_protocol = SHARED;
      //CPU2
      cache_line_L1_CPU2[i].valid = true;
      cache_line_L1_CPU2[i].tag = tag_L1_i;
      cache_line_L1_CPU2[i].dirty = 0;
      cache_line_L1_CPU2[i].rp_value = i;
      cache_line_L1_CPU2[i].coherence_protocol = SHARED;
      while (cache_line_L1_CPU1[i].tag == tag_L1) {
        cache_line_L1_CPU1[i].tag = rand()%4096;
        cache_line_L1_CPU2[i].tag = cache_line_L1_CPU1[i].tag;
      }
    }
    //-------------------------------------------------------------------------------------------------
    //MODIFIED test
    loadstore = true;
    //cout << "coco: " << msi_mesi<< endl;
    status = lru_multilevel_cache (idx_L1,
                              idx_L2,
                              tag_L1,
                              tag_L2,

                              associativity_L1,
                              associativity_L2,

                              loadstore,
                              msi_mesi,

                              cache_line_L1_CPU2,
                              cache_line_L1_CPU1,
                              cache_line_L2,

                              &result_for_L1_CPU2,
                              &result_for_L2,

                              L1_stats_count_CPU2,
                              L1_stats_count_CPU1,
                              L2_stats_count,
                              debug
    );

    for ( i =  0; i < associativity_L1; i++) {
      if (cache_line_L1_CPU2[i].tag == tag_L1) {
        cohe_test_CPU1_M_2 = cache_line_L1_CPU1[i].coherence_protocol;
        cohe_test_CPU2_M_2 = cache_line_L1_CPU2[i].coherence_protocol;
      }
    }

    //SHARED test
    loadstore = false;
    status = lru_multilevel_cache (idx_L1,
                              idx_L2,
                              tag_L1,
                              tag_L2,

                              associativity_L1,
                              associativity_L2,

                              loadstore,
                              msi_mesi,

                              cache_line_L1_CPU1,
                              cache_line_L1_CPU2,
                              cache_line_L2,

                              &result_for_L1_CPU1,
                              &result_for_L2,

                              L1_stats_count_CPU1,
                              L1_stats_count_CPU2,
                              L2_stats_count,
                              debug
    );

    for ( i =  0; i < associativity_L1; i++) {
      if (cache_line_L1_CPU1[i].tag == tag_L1) {
        cohe_test_CPU1_S = cache_line_L1_CPU1[i].coherence_protocol;
        cohe_test_CPU2_S = cache_line_L1_CPU2[i].coherence_protocol;
      }
    }

    /*
    Test2: CPU1 write + CPU2 read
    Stimuli
    1. Choose a random associativity
    2. Choose a random address (AddressA)
    3. Fill a l1 CPU 1 cache line with random addresses, making sure AddressA is not added.
    4. Fill a l1 CPU 2 cache line with shared addresses, making sure AddressA is not added.
    6. Write Address A in CPU 1. Check 1,2.
    5. Read Address A in CPU 2. Check 3,4.

    Checks
    1. Check coherence protocol status in CPU 1 changed to MODIFIED.
    2. Check coherence protocol status in CPU 2 changed from SHARED to INVALID.
    3. Check coherence protocol status in CPU 1 changed from MODIFIED to SHARED.
    4. Check coherence protocol status in CPU 2 changed from INVALID to SHARED.
    */

    EXPECT_EQ(status, 0);

    expected_cohe_prot_for_CPU1 = INVALID;
    expected_cohe_prot_for_CPU2 = MODIFIED;
    EXPECT_EQ(cohe_test_CPU1_M_2, expected_cohe_prot_for_CPU1);
    EXPECT_EQ(cohe_test_CPU2_M_2, expected_cohe_prot_for_CPU2);

    expected_cohe_prot_for_CPU1 = SHARED;
    expected_cohe_prot_for_CPU2 = SHARED;
    EXPECT_EQ(cohe_test_CPU1_S, expected_cohe_prot_for_CPU1);
    EXPECT_EQ(cohe_test_CPU2_S, expected_cohe_prot_for_CPU2);

  }  //ends test for
}// end Test2





/*
 * Gtest main function: Generates random seed, if not provided,
 * parses DEBUG flag, and execute the test suite
 */
int main(int argc, char **argv) {
  int argc_to_pass = 0;
  char **argv_to_pass = NULL;
  int seed = 0;

  /* Generate seed */
  seed = time(NULL) & 0xffff;

  /* Parse arguments looking if random seed was provided */
  argv_to_pass = (char **)calloc(argc + 1, sizeof(char *));

  for (int i = 0; i < argc; i++){
    std::string arg = std::string(argv[i]);

    if (!arg.compare(0, 20, "--gtest_random_seed=")){
      seed = atoi(arg.substr(20).c_str());
      continue;
    }
    argv_to_pass[argc_to_pass] = strdup(arg.c_str());
    argc_to_pass++;
  }

  /* Init Gtest */
  ::testing::GTEST_FLAG(random_seed) = seed;
  testing::InitGoogleTest(&argc, argv_to_pass);

  /* Print seed for debug */
  printf(YEL "Random seed %d \n",seed);
  srand(seed);

  /* Parse for debug env variable */
  //debug_on = 1; --> doesn't work
  get_env_var("TEST_DEBUG", &debug_on);

  /* Execute test */
  return RUN_ALL_TESTS();

  /* Free memory */
  free(argv_to_pass);

  return 0;
}


/*
for correct L2 filling:


struct entry cache_line_L1_CPU1[associativity_L1];
struct entry cache_line_L1_CPU2[associativity_L1];
struct entry cache_line_L2[associativity_L2];

int rand_tag_for_L2 =  pow(2,tag_size_L2);

DEBUG(Checking CPU coherence protocol changes);
long address_got_from_L1_i = 0;
int tag_L1_i;
// int idx_L2_i;
// int tag_L2_i;
//tag and idx from the L1 replaced address
// int* tag_L2_address_A_i = new int;
// int* idx_L2_address_A_i = new int;
//starts test for
for (i = 0 ; i < 1; i++){
  // loadstore = (bool)rand()%2;
  // if (debug) {
  //   cout << "loadstore: " << loadstore << endl;
  // }
  /* Fill cache line
  for ( i =  0; i < associativity_L1; i++) {
    //-------------------------------------------------------------------------------------------------
    tag_L1_i = rand()%4096;

    //CPU1
    cache_line_L1_CPU1[i].valid = true;
    cache_line_L1_CPU1[i].tag = tag_L1_i;
    cache_line_L1_CPU1[i].dirty = 0;
    cache_line_L1_CPU1[i].rp_value = i;
    cache_line_L1_CPU1[i].coherence_protocol = SHARED;
    //CPU2
    cache_line_L1_CPU2[i].valid = true;
    cache_line_L1_CPU2[i].tag = tag_L1_i;
    cache_line_L1_CPU2[i].dirty = 0;
    cache_line_L1_CPU2[i].rp_value = i;
    cache_line_L1_CPU2[i].coherence_protocol = SHARED;
    while (cache_line_L1_CPU1[i].tag == tag_L1) {
      cache_line_L1_CPU1[i].tag = rand()%4096;
      cache_line_L1_CPU2[i].tag = cache_line_L1_CPU1[i].tag;
    }
    if (i == 0) {
      tag_check_L1_LRU = cache_line_L1_CPU1[i].tag;
      idx_check_L1_LRU = idx_L1;
      dirty_check_L1_LRU = cache_line_L1_CPU1[i].dirty;
    }



    // //--------------------------------get address A--------------------------------------------
    // //address mask, puts tag and idx here
    // address_got_from_L1_i = 0;
    //
    // //concatenates tag bits
    //
    // address_got_from_L1_i = address_got_from_L1_i << tag_size_L1;
    // if (debug && extra_debug) {
    //   cout << "address masked L1: " << address_got_from_L1_i << endl;
    // }
    //
    // address_got_from_L1_i = address_got_from_L1_i | tag_L1_i;
    // if (debug && extra_debug) {
    //   cout << "address | tag L1: " << address_got_from_L1_i << endl;
    // }
    // //concatenates idx bits
    // address_got_from_L1_i = address_got_from_L1_i << idx_size_L1;
    // if (debug && extra_debug) {
    //   cout << "address masked with tag L1: " << address_got_from_L1_i << endl;
    // }
    //
    // address_got_from_L1_i = address_got_from_L1_i | idx_L1;
    // if (debug && extra_debug) {
    //   cout << "address masked with tag | index L1: " << address_got_from_L1_i << endl;
    // }
    //
    // //concatenates offset bits
    // address_got_from_L1_i = address_got_from_L1_i << offset_size;
    // if (debug) {
    //   cout << "address obtained: L1 original  " << address_got_from_L1_i << endl;
    // }
    // //-----------------------------------get address block end-----------------------------------------
    //
    //
    // //to replicate address and see if it matches
    // if (debug) {
    //   address_tag_idx_get(    address_got_from_L1_i,
    //                           tag_size_L1,
    //                           idx_size_L1,
    //                           offset_size,
    //                           idx_L2_address_A_i,
    //                           tag_L2_address_A_i
    //   );
    //   cout << "\n\n tag L1 replicate: " << *tag_L2_address_A_i << endl;
    //   cout << " idx L1 replicate: " << *idx_L2_address_A_i << "\n\n";
    // }
    //
    // //gets new tag and idx for L2, then use them to add the evicted element
    // address_tag_idx_get(    address_got_from_L1_i,
    //                         tag_size_L2,
    //                         idx_size_L2,
    //                         offset_size,
    //                         idx_L2_address_A_i,
    //                         tag_L2_address_A_i
    // );
    // idx_L2_i = *idx_L2_address_A_i;
    // tag_L2_i = *tag_L2_address_A_i;
    //
    // /* Fill L2, L1 spaces
    // cache_line_L2[i].valid = true;
    // cache_line_L2[i].tag = tag_L2_i;
    // cache_line_L2[i].dirty = 0;
    // cache_line_L2[i].rp_value = i;
    // cache_line_L2[i].coherence_protocol = INVALID;
    //
    // if (i == 0) {
    //   tag_check_L2_LRU = cache_line_L2[i].tag;
    //   idx_check_L2_LRU = i;
    //   dirty_check_L2_LRU = cache_line_L2[i].dirty;
    // }


  }
  //-------------------------------------------------------------------------------------------------

  // /* Fill L2
  // for ( j =  i-1; j < associativity_L2; j++) {
  //   cache_line_L2[i].valid = true;
  //   cache_line_L2[i].tag = rand()%rand_tag_for_L2;
  //   cache_line_L2[i].dirty = 0;
  //   cache_line_L2[i].rp_value = j;
  //   cache_line_L2[i].coherence_protocol = INVALID;
  //   while (cache_line_L2[j].tag == tag_L2) {
  //     cache_line_L2[j].tag = rand()%rand_tag_for_L2;
  //   }
  // }



  // for ( int i =  0; i < associativity_L1; i++) {
  //   cout << "IN_lru_victim_cache tag filled: " << cache_L1_blocks[i].tag << endl;
  // }


*/
