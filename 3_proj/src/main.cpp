#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>



#define HIT_TIME 1
#define MISS_PENALTY 20
#define KB 1024

/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  //printf("Do something :), don't forget to keep track of execution time");

  // -------------------------------variable declaration---------------------------------
  /* Parse argruments */
  int cachesize_kb = stoi(argv[2]);
  int blocksize_bytes = stoi(argv[4]);
  int associativity = stoi(argv[6]);
  string coher_prot = argv[8];
  string rp = "lru";
  cout << "cache_size: " << cachesize_kb << " blocksize_bytes: " << blocksize_bytes << " associativity: " << associativity << " coherence protocol: " << coher_prot << endl;
  //variables for  field_size_get_cache_build function
  int* tag_size = new int;
  int* idx_size = new int;
  int* offset_size = new int;
  //entry*** l1_cache = new entry**;
  //variables for  field_size_get_cache_build function
  long address;
  int* idx = new int;
  int* tag = new int;

  //variables for rp functions
  bool loadstore;
  operation_result* op_result_l1_CPU1 = new operation_result;
  //op_result for CPU2
  operation_result* op_result_l1_CPU2 = new operation_result;
  //op_result for L2
  operation_result* op_result_l2 = new operation_result;

  /*
  LS_miss_hit_eviction_count[0] = LOAD_MISS;
  LS_miss_hit_eviction_count[1] = STORE_MISS;
  LS_miss_hit_eviction_count[2] = LOAD_HIT;
  LS_miss_hit_eviction_count[3] = STORE_HIT;
  LS_miss_hit_eviction_count[4] = dirty_evictions;
  */
  int* LS_miss_hit_eviction_inv_count_for_L1_CPU1 = new int[6];
  int* LS_miss_hit_eviction_inv_count_for_L1_CPU2 = new int[6];
  int* l2_stats_count = new int[5];

  //initialize counters:
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[0] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[1] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[2] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[3] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[4] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[5] = 0;

  LS_miss_hit_eviction_inv_count_for_L1_CPU2[0] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[1] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[2] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[3] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[4] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[5] = 0;

  //initialize counters:
  l2_stats_count[0] = 0;
  l2_stats_count[1] = 0;
  l2_stats_count[2] = 0;
  l2_stats_count[3] = 0;
  l2_stats_count[4] = 0;


  bool debug = 0;

  //variables to read from traces
  string hashtag;
  int instruction_count_in;
  int instruction_count = 0;

  // -------------------------------function initialization---------------------------------
  int status; // says if the function returns an error
  status =  field_size_get_cache_build(cachesize_kb,
                     associativity,
                     blocksize_bytes,
                     tag_size,
                     idx_size,
                     offset_size,
                     //l1_cache,
                     rp);
  if (status == ERROR) {
    cout << "arguments were incorrect, program will close." << endl;
    exit(0);
  }
  cout << "offset_size l1: " << *offset_size << endl;


  // -------------------------------function initialization for L2---------------------------------
  //L2 size is 4 times L1's size and 2 times its associativity
  int l2_cache_size_kb = cachesize_kb*4;
  int l2_associativity = associativity*2;
  int* l2_tag_size = new int;
  int* l2_idx_size = new int;
  int* l2_idx = new int;
  int* l2_tag = new int;
  status =  field_size_get_cache_build(l2_cache_size_kb,
                     l2_associativity,
                     blocksize_bytes,
                     l2_tag_size,
                     l2_idx_size,
                     offset_size,
                     //l1_cache,
                     rp);
  if (status == ERROR) {
    cout << "arguments were incorrect, program will close." << endl;
    exit(0);
  }
  cout << "offset_size l2: " << *offset_size << endl;


  //------------------------------------------------------------------------------------------------
  entry** l1_cache_CPU1 = cache_build(*idx_size, associativity, rp);
  entry** l1_cache_CPU2 = cache_build(*idx_size, associativity, rp);
  /*
  Se deber´a simular dos niveles de cach´e: primer nivel (L1) y segundo nivel
  (L2). El usuario podr´a deﬁnir el taman˜o y la asociatividad de L1, mientras
  que, el taman˜o y asociatividad de L2 ser´an deﬁnidos a partir de las caracter´ısticas del
  cach´e de primer nivel. El taman˜o del cach´e de segundo nivel deber´a ser cuatro veces
  el taman˜o de L1 y su asociatividad se deﬁnir´a como el doble de la del primer nivel.
  Adem´as, los niveles L1 y L2 utilizan una pol´ıtica de remplazo LRU, son inclusivos y
  write-throuth, mientras que entre L2 y memoria se utiliza una pol´ıtica writeback
  */

  entry** l2_cache = cache_build(*l2_idx_size, l2_associativity, rp);


  // set coherence protocol:
  bool msi_mesi;
  if (coher_prot == "msi") {
    msi_mesi = false;
  }else{
    msi_mesi = true;
  }

  /* Get trace's lines and start your simulation */
  //if cin reads nothing last line, ends while
  //(eof = end of line)
  int access_count = 0;
  cout << "\n-->" << access_count << endl << endl;
  while (!cin.eof() ) { //|| hashtag != "done") {
    cin >> hashtag;
    if (hashtag == "done") {
      break;
    }
    cin >> loadstore;

    cin >> hex >> address;

    cin >> dec >> instruction_count_in;
    instruction_count = instruction_count + instruction_count_in;

    //
    // if (access_count == 518269) {
    //   cout << " IC_in: " << instruction_count_in << " IC_sum: " << instruction_count <<endl;
    //   cout << "hashtag: " << hashtag << " LS: " << loadstore << " address: " << address << endl;
    // }


    address_tag_idx_get(address,
                        *tag_size,
                        *idx_size,
                        *offset_size,
                        idx,
                        tag);

    address_tag_idx_get(address,
                        *l2_tag_size,
                        *l2_idx_size,
                        *offset_size,
                        l2_idx,
                        l2_tag);



    if (access_count % 4 == 0) {

      status = lru_multilevel_cache (*idx,
                                    *l2_idx,
                                    *tag,
                                    *l2_tag,

                                    associativity,
                                    l2_associativity, //number of entries if it is 0

                                    loadstore,
                                    msi_mesi,

                                    l1_cache_CPU1[*idx],
                                    l1_cache_CPU2[*idx],
                                    l2_cache[*l2_idx],

                                    op_result_l1_CPU1,
                                    op_result_l2,

                                    LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                                    LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                                    l2_stats_count
      );

    } else{
      // cout << "pepe";
      status = lru_multilevel_cache (*idx,
                                *l2_idx,
                                *tag,
                                *l2_tag,

                                associativity,
                                l2_associativity, //number of entries if it is 0

                                loadstore,
                                msi_mesi,

                                l1_cache_CPU2[*idx],
                                l1_cache_CPU1[*idx],
                                l2_cache[*l2_idx],

                                op_result_l1_CPU2,
                                op_result_l2,

                                LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                                LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                                l2_stats_count
      );




    }

    // cout << "\n\n\tFOR L1\n";
    // cout << "LOAD_MISS: " << LS_miss_hit_eviction_count[0] << endl;
    // cout << "STORE_MISS: " << LS_miss_hit_eviction_count[1] << endl;
    // cout << "LOAD_HIT: " << LS_miss_hit_eviction_count[2] << endl;
    // cout << "STORE_HIT: " << LS_miss_hit_eviction_count[3] << endl;
    // cout << "dirty_evictions: " << LS_miss_hit_eviction_count[4] << endl;
    //
    // cout << "\n\n\tFOR L2\n";
    // cout << "LOAD_MISS: " << l2_stats_count[0] << endl;
    // cout << "STORE_MISS: " << l2_stats_count[1] << endl;
    // cout << "LOAD_HIT: " << l2_stats_count[2] << endl;
    // cout << "STORE_HIT: " << l2_stats_count[3] << endl;
    // cout << "dirty_evictions: " << l2_stats_count[4] << endl;

    access_count = access_count + 1;

  }// end of while

  cout << "\n-->" << access_count << endl << endl;

  // //Statistics counts:
  // int total_hits_L1;
  // int total_misses_L1;
  // int accesses_L1;
  // int miss_rate_L1;
  // //int hit_rate_L1;
  // //int read_miss_rate;
  // //int CPU_time;
  // //int AMAT;
  //
  //
  // // int* LS_miss_hit_eviction_count = new int[5];
  // // int* l2_stats_count = new int[5];
  // // int* vc_stats_count = new int[5];
  //
  //
  // total_hits_L1 = LS_miss_hit_eviction_count[2] + LS_miss_hit_eviction_count[3];
  // total_misses_L1 = LS_miss_hit_eviction_count[0] + LS_miss_hit_eviction_count[1];
  // accesses_L1 = total_hits_L1 + total_misses_L1;
  //
  // //Stats
  // miss_rate_L1 = double(total_misses_L1) / double(accesses_L1);
  // //hit_rate_L1 = double(total_hits_L1) / double(accesses_L1);
  // //read_miss_rate = double(LS_miss_hit_eviction_count[0]) / double(total_misses_L1);
  // //CPU_time = instruction_count - LS_miss_hit_eviction_count[0] + (HIT_TIME + MISS_PENALTY)*LS_miss_hit_eviction_count[0];
  // //AMAT = HIT_TIME + miss_rate*MISS_PENALTY;


  //Statistics counts:
  //for L1 CPU1
  int total_hits_L1_CPU1;
  int total_misses_L1_CPU1;
  int accesses_L1_CPU1;
  double miss_rate_L1_CPU1;

  //for L1 CPU2
  int total_hits_L1_CPU2;
  int total_misses_L1_CPU2;
  int accesses_L1_CPU2;
  double miss_rate_L1_CPU2;

  //for L2
  int total_hits_L2;
  int total_misses_L2;
  int accesses_L2;
  double miss_rate_L2;

  //for combined Stats
  int total_hits_l2l1;
  int total_misses_l2l1;
  int accesses_l2l1;
  double miss_rate_l2l1;

  //for L1 CPU1
  total_hits_L1_CPU1 = LS_miss_hit_eviction_inv_count_for_L1_CPU1[2] + LS_miss_hit_eviction_inv_count_for_L1_CPU1[3];
  total_misses_L1_CPU1 = LS_miss_hit_eviction_inv_count_for_L1_CPU1[0] + LS_miss_hit_eviction_inv_count_for_L1_CPU1[1];
  accesses_L1_CPU1 = total_hits_L1_CPU1 + total_misses_L1_CPU1;

  miss_rate_L1_CPU1 = double(total_misses_L1_CPU1) / double(accesses_L1_CPU1);

  //for L1 CPU2
  total_hits_L1_CPU2 = LS_miss_hit_eviction_inv_count_for_L1_CPU2[2] + LS_miss_hit_eviction_inv_count_for_L1_CPU2[3];
  total_misses_L1_CPU2 = LS_miss_hit_eviction_inv_count_for_L1_CPU2[0] + LS_miss_hit_eviction_inv_count_for_L1_CPU2[1];
  accesses_L1_CPU2 = total_hits_L1_CPU2 + total_misses_L1_CPU2;

  miss_rate_L1_CPU2 = double(total_misses_L1_CPU2) / double(accesses_L1_CPU2);

  //for L2
  total_hits_L2 = l2_stats_count[2] + l2_stats_count[3];
  total_misses_L2 = l2_stats_count[0] + l2_stats_count[1];
  accesses_L2 = total_hits_L2 + total_misses_L2;

  miss_rate_L2 = double(total_misses_L2) / double(accesses_L2);

  // //for both combined
  // total_hits_l2l1 = total_hits_L1 + total_hits_L2;
  // total_misses_l2l1 = total_misses_L1 + total_misses_L2;
  //
  // accesses_l2l1 = accesses_L1 + accesses_L2;
  // miss_rate_l2l1 = double(total_misses_l2l1) / double(accesses_l2l1);


  cout << endl;
  cout << "*****************************************" << endl;
  cout << "Cache Parameters" << endl;
  cout << "*****************************************" << endl;
  cout << "L1 Cache Size (KB): " << ".........." << cachesize_kb << endl;
  cout << "L2 Cache Size (KB): " << ".........." << l2_cache_size_kb << endl;
  cout << "Cache L1 Associativity: " << "......" << associativity << endl;
  cout << "Cache L2 Associativity: " << "......" << l2_associativity << endl;
  cout << "Cache Block Size (bytes):" << "....." << blocksize_bytes << endl;
  cout << "Coherence Protocol: " << ".........." << coher_prot << endl;

  /* Print Statistics */

  cout << "*****************************************" << endl;
  cout << "Simulation results:" << endl;
  cout << "*****************************************" << endl;
  //cout << "CPU time (cycles):" << "........." << CPU_time << endl;
  //cout << "AMAT(cycles):" << ".............." << AMAT << endl;
  cout << "Overall miss rate" << ".........." << miss_rate_l2l1 << endl;
  cout << "CPU1 L1 miss rate:" << "........." << miss_rate_L1_CPU1 << endl;
  cout << "CPU2 L1 miss rate:" << "........." << miss_rate_L1_CPU2 << endl;
  cout << "Coherence Invalidation CPU1:" << "---" << LS_miss_hit_eviction_inv_count_for_L1_CPU1[5] << endl;
  cout << "Coherence Invalidation CPU2:" << "---" << LS_miss_hit_eviction_inv_count_for_L1_CPU2[5] << endl;
  //cout << "L2 miss rate:" << ".............." << miss_rate_L2 << endl;
  //cout << "Misses (L1):" << "..............." << total_misses_L1 << endl;
  //cout << "Hits (L1):" << "................." << total_hits_L1 << endl;
  //cout << "Misses (L2):" << "..............." << total_misses_L2 << endl;
  //cout << "Hits (L2):" << "................." << total_hits_L2 << endl;
  //cout << "Dirty evictions (L2):" << "......" << l2_stats_count[4] << endl;
  //cout << "Load misses:" << "..............." << LS_miss_hit_eviction_count[0] << endl;
  //cout << "Store misses:" << ".............." << LS_miss_hit_eviction_count[1] << endl;
  //cout << "Load hits:" << "................." << LS_miss_hit_eviction_count[2] << endl;
  //cout << "Store hits:" << "................" << LS_miss_hit_eviction_count[3] << endl;
  //cout << "Total memory accesses:" << "....." << accesses << endl;
  cout << "*****************************************" << endl;
  /*
  LS_miss_hit_eviction_count[0] = LOAD_MISS;
  LS_miss_hit_eviction_count[1] = STORE_MISS;
  LS_miss_hit_eviction_count[2] = LOAD_HIT;
  LS_miss_hit_eviction_count[3] = STORE_HIT;
  LS_miss_hit_eviction_count[4] = dirty_evictions;
  */


  return 0;
}

/*

LS Direccion IC
# 0 7fffed80 1
# 0 10010000 10
# 0 10010060 3
# 0 10010030 4
# 0 10010004 6
# 0 10010064 3
# 0 10010034 4

*/
