#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "L1cache.h"
#include <ctime>



#include <pthread.h>

using namespace std;
#define KB 1024
#define ADDRSIZE 3
#define HIT_TIME 1
#define MISS_PENALTY 20
#define KB 1024

/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  //printf("Do something :), don't forget to keep track of execution time");
  unsigned ti, tf;
  ti = clock();
  // -------------------------------variable declaration---------------------------------
  /* Parse argruments */
  int cachesize_kb = stoi(argv[2]);
  int blocksize_bytes = stoi(argv[4]);
  int associativity = stoi(argv[6]);
  string coher_prot = argv[8];
  string rp = "lru";
  cout << "cache_size: " << cachesize_kb << " blocksize_bytes: " << blocksize_bytes << " associativity: " << associativity << " coherence protocol: " << coher_prot << endl;
  //variables for  field_size_get_cache_build function
  int* tag_size = new int;
  int* idx_size = new int;
  int* offset_size = new int;
  //entry*** l1_cache = new entry**;
  //variables for  field_size_get_cache_build function
  long address;
  int* idx = new int;
  int* tag = new int;

  //variables for rp functions
  bool loadstore;
  operation_result* op_result_l1_CPU1 = new operation_result;
  //op_result for CPU2
  operation_result* op_result_l1_CPU2 = new operation_result;
  //op_result for L2
  operation_result* op_result_l2 = new operation_result;

  /*
  LS_miss_hit_eviction_count[0] = LOAD_MISS;
  LS_miss_hit_eviction_count[1] = STORE_MISS;
  LS_miss_hit_eviction_count[2] = LOAD_HIT;
  LS_miss_hit_eviction_count[3] = STORE_HIT;
  LS_miss_hit_eviction_count[4] = dirty_evictions;
  */
  int* LS_miss_hit_eviction_inv_count_for_L1_CPU1 = new int[6];
  int* LS_miss_hit_eviction_inv_count_for_L1_CPU2 = new int[6];
  int* l2_stats_count = new int[5];

  //initialize counters:
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[0] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[1] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[2] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[3] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[4] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU1[5] = 0;

  LS_miss_hit_eviction_inv_count_for_L1_CPU2[0] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[1] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[2] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[3] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[4] = 0;
  LS_miss_hit_eviction_inv_count_for_L1_CPU2[5] = 0;

  //initialize counters:
  l2_stats_count[0] = 0;
  l2_stats_count[1] = 0;
  l2_stats_count[2] = 0;
  l2_stats_count[3] = 0;
  l2_stats_count[4] = 0;


  bool debug = 0;

  //variables to read from traces
  string hashtag;
  int instruction_count_in;
  int instruction_count = 0;

  // -------------------------------function initialization---------------------------------
  int status; // says if the function returns an error



 /*
  *
  * 
  * 
  * 
  *                  FIELD SIZE GETS! 
  * 
  * 
  * 
  * 
  */
  // Thread initializations
        pthread_t threads[2];
        pthread_attr_t attr;
        void *status_t;
        //Joinable Threads
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);


  struct field_size_data fs_data[2];
  int rc;
  int rc2;

    // Llenamos vector de datos para thread 1
  fs_data[0].cachesize_kb = cachesize_kb;
  fs_data[0].associativity = associativity;
  fs_data[0].blocksize_bytes = blocksize_bytes;
  
  fs_data[0].idx_size = idx_size;
  fs_data[0].offset_size = offset_size;
  fs_data[0].tag_size = tag_size;

  fs_data[0].rp = rp;





  cout << "offset_size l1: " << *offset_size << endl;





  // -------------------------------function initialization for L2---------------------------------
  //L2 size is 4 times L1's size and 2 times its associativity
  int l2_cache_size_kb = cachesize_kb*4;
  int l2_associativity = associativity*2;
  int* l2_tag_size = new int;
  int* l2_idx_size = new int;
  int* l2_idx = new int;
  int* l2_tag = new int;

    // Datos para el thread 2

  fs_data[1].cachesize_kb = l2_cache_size_kb;
  fs_data[1].associativity = l2_associativity;
  fs_data[1].blocksize_bytes = blocksize_bytes;

  fs_data[1].idx_size = l2_idx_size;
  fs_data[1].offset_size = offset_size;
  fs_data[1].tag_size = l2_tag_size;

  fs_data[0].rp = rp;
  //--------------Threads----------------------
  rc = pthread_create(&threads[0], &attr, field_size_get_cache_build, (void *)&fs_data[0]);
  rc = pthread_create(&threads[1], &attr, field_size_get_cache_build, (void*)&fs_data[1]);

  //--------------Joining Threads------------

  rc = pthread_join(threads[0], &status_t);
  rc = pthread_join(threads[1], &status_t);



  cout << "offset_size l2: " << *offset_size << endl;


  //------------------------------------------------------------------------------------------------
  entry** l1_cache_CPU1 = cache_build(*idx_size, associativity, rp);
  entry** l1_cache_CPU2 = cache_build(*idx_size, associativity, rp);
  /*
  Se deber´a simular dos niveles de cach´e: primer nivel (L1) y segundo nivel
  (L2). El usuario podr´a deﬁnir el taman˜o y la asociatividad de L1, mientras
  que, el taman˜o y asociatividad de L2 ser´an deﬁnidos a partir de las caracter´ısticas del
  cach´e de primer nivel. El taman˜o del cach´e de segundo nivel deber´a ser cuatro veces
  el taman˜o de L1 y su asociatividad se deﬁnir´a como el doble de la del primer nivel.
  Adem´as, los niveles L1 y L2 utilizan una pol´ıtica de remplazo LRU, son inclusivos y
  write-throuth, mientras que entre L2 y memoria se utiliza una pol´ıtica writeback
  */

  entry** l2_cache = cache_build(*l2_idx_size, l2_associativity, rp);


  // set coherence protocol:
  bool msi_mesi;
  if (coher_prot == "msi") {
    msi_mesi = false;
  }else{
    msi_mesi = true;
  }

  /* Get trace's lines and start your simulation */
  //if cin reads nothing last line, ends while
  //(eof = end of line)
  int access_count = 0;
    struct tag_idx_data ti_data[2];

  cout << "\n-->" << access_count << endl << endl;
  while (!cin.eof() ) { //|| hashtag != "done") {
    cin >> hashtag;
    if (hashtag == "done") {
      break;
    }
    cin >> loadstore;

    cin >> hex >> address;

    cin >> dec >> instruction_count_in;
    instruction_count = instruction_count + instruction_count_in;

    //
    // if (access_count == 518269) {
    //   cout << " IC_in: " << instruction_count_in << " IC_sum: " << instruction_count <<endl;
    //   cout << "hashtag: " << hashtag << " LS: " << loadstore << " address: " << address << endl;
    // }


    /*
     *
     * 
     *                      ADDRESS TAG IDX GET
     * 
     *  
     */
    // "Empaquetado" de datos en struct respectivo
    ti_data[0].address = address;
    ti_data[0].tag_size = *tag_size;
    ti_data[0].idx_size = *idx_size;
    ti_data[0].offset_size = *offset_size;
    ti_data[0].idx = idx;
    ti_data[0].tag = tag;


    ti_data[1].address = address;
    ti_data[1].tag_size = *l2_tag_size;
    ti_data[1].idx_size = *l2_idx_size;
    ti_data[1].offset_size = *offset_size;
    ti_data[1].idx = l2_idx;
    ti_data[1].tag =  l2_tag;


    rc = pthread_create(&threads[0], &attr, address_tag_idx_get, (void *)&ti_data[0]);

    rc = pthread_create(&threads[1], &attr, address_tag_idx_get, (void*)&ti_data[1]);
    
    // Join de los threads
    rc = pthread_join (threads[0], &status_t ); 
    rc = pthread_join (threads[1], &status_t ); 

    if (access_count % 4 == 0) {

      status = lru_multilevel_cache (*idx,
                                    *l2_idx,
                                    *tag,
                                    *l2_tag,

                                    associativity,
                                    l2_associativity, //number of entries if it is 0

                                    loadstore,
                                    msi_mesi,

                                    l1_cache_CPU1[*idx],
                                    l1_cache_CPU2[*idx],
                                    l2_cache[*l2_idx],

                                    op_result_l1_CPU1,
                                    op_result_l2,

                                    LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                                    LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                                    l2_stats_count
      );

    } else{
      // cout << "pepe";
      status = lru_multilevel_cache (*idx,
                                *l2_idx,
                                *tag,
                                *l2_tag,

                                associativity,
                                l2_associativity, //number of entries if it is 0

                                loadstore,
                                msi_mesi,

                                l1_cache_CPU2[*idx],
                                l1_cache_CPU1[*idx],
                                l2_cache[*l2_idx],

                                op_result_l1_CPU2,
                                op_result_l2,

                                LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                                LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                                l2_stats_count
      );




    }

    // cout << "\n\n\tFOR L1\n";
    // cout << "LOAD_MISS: " << LS_miss_hit_eviction_count[0] << endl;
    // cout << "STORE_MISS: " << LS_miss_hit_eviction_count[1] << endl;
    // cout << "LOAD_HIT: " << LS_miss_hit_eviction_count[2] << endl;
    // cout << "STORE_HIT: " << LS_miss_hit_eviction_count[3] << endl;
    // cout << "dirty_evictions: " << LS_miss_hit_eviction_count[4] << endl;
    //
    // cout << "\n\n\tFOR L2\n";
    // cout << "LOAD_MISS: " << l2_stats_count[0] << endl;
    // cout << "STORE_MISS: " << l2_stats_count[1] << endl;
    // cout << "LOAD_HIT: " << l2_stats_count[2] << endl;
    // cout << "STORE_HIT: " << l2_stats_count[3] << endl;
    // cout << "dirty_evictions: " << l2_stats_count[4] << endl;

    access_count = access_count + 1;

  }// end of while

  cout << "\n-->" << access_count << endl << endl;

  // //Statistics counts:
  // int total_hits_L1;
  // int total_misses_L1;
  // int accesses_L1;
  // int miss_rate_L1;
  // //int hit_rate_L1;
  // //int read_miss_rate;
  // //int CPU_time;
  // //int AMAT;
  //
  //
  // // int* LS_miss_hit_eviction_count = new int[5];
  // // int* l2_stats_count = new int[5];
  // // int* vc_stats_count = new int[5];
  //
  //
  // total_hits_L1 = LS_miss_hit_eviction_count[2] + LS_miss_hit_eviction_count[3];
  // total_misses_L1 = LS_miss_hit_eviction_count[0] + LS_miss_hit_eviction_count[1];
  // accesses_L1 = total_hits_L1 + total_misses_L1;
  //
  // //Stats
  // miss_rate_L1 = double(total_misses_L1) / double(accesses_L1);
  // //hit_rate_L1 = double(total_hits_L1) / double(accesses_L1);
  // //read_miss_rate = double(LS_miss_hit_eviction_count[0]) / double(total_misses_L1);
  // //CPU_time = instruction_count - LS_miss_hit_eviction_count[0] + (HIT_TIME + MISS_PENALTY)*LS_miss_hit_eviction_count[0];
  // //AMAT = HIT_TIME + miss_rate*MISS_PENALTY;


  //Statistics counts:
  //for L1 CPU1
  int total_hits_L1_CPU1;
  int total_misses_L1_CPU1;
  int accesses_L1_CPU1;
  double miss_rate_L1_CPU1;

  //for L1 CPU2
  int total_hits_L1_CPU2;
  int total_misses_L1_CPU2;
  int accesses_L1_CPU2;
  double miss_rate_L1_CPU2;

  //for L2
  int total_hits_L2;
  int total_misses_L2;
  int accesses_L2;
  double miss_rate_L2;

  //for combined Stats
  int total_hits_l2l1;
  int total_misses_l2l1;
  int accesses_l2l1;
  double miss_rate_l2l1;

  //for L1 CPU1
  total_hits_L1_CPU1 = LS_miss_hit_eviction_inv_count_for_L1_CPU1[2] + LS_miss_hit_eviction_inv_count_for_L1_CPU1[3];
  total_misses_L1_CPU1 = LS_miss_hit_eviction_inv_count_for_L1_CPU1[0] + LS_miss_hit_eviction_inv_count_for_L1_CPU1[1];
  accesses_L1_CPU1 = total_hits_L1_CPU1 + total_misses_L1_CPU1;

  miss_rate_L1_CPU1 = double(total_misses_L1_CPU1) / double(accesses_L1_CPU1);

  //for L1 CPU2
  total_hits_L1_CPU2 = LS_miss_hit_eviction_inv_count_for_L1_CPU2[2] + LS_miss_hit_eviction_inv_count_for_L1_CPU2[3];
  total_misses_L1_CPU2 = LS_miss_hit_eviction_inv_count_for_L1_CPU2[0] + LS_miss_hit_eviction_inv_count_for_L1_CPU2[1];
  accesses_L1_CPU2 = total_hits_L1_CPU2 + total_misses_L1_CPU2;

  miss_rate_L1_CPU2 = double(total_misses_L1_CPU2) / double(accesses_L1_CPU2);

  //for L2
  total_hits_L2 = l2_stats_count[2] + l2_stats_count[3];
  total_misses_L2 = l2_stats_count[0] + l2_stats_count[1];
  accesses_L2 = total_hits_L2 + total_misses_L2;

  miss_rate_L2 = double(total_misses_L2) / double(accesses_L2);

  // //for both combined
  // total_hits_l2l1 = total_hits_L1 + total_hits_L2;
  // total_misses_l2l1 = total_misses_L1 + total_misses_L2;
  //
  // accesses_l2l1 = accesses_L1 + accesses_L2;
  // miss_rate_l2l1 = double(total_misses_l2l1) / double(accesses_l2l1);


  cout << endl;
  cout << "*****************************************" << endl;
  cout << "Cache Parameters" << endl;
  cout << "*****************************************" << endl;
  cout << "L1 Cache Size (KB): " << ".........." << cachesize_kb << endl;
  cout << "L2 Cache Size (KB): " << ".........." << l2_cache_size_kb << endl;
  cout << "Cache L1 Associativity: " << "......" << associativity << endl;
  cout << "Cache L2 Associativity: " << "......" << l2_associativity << endl;
  cout << "Cache Block Size (bytes):" << "....." << blocksize_bytes << endl;
  cout << "Coherence Protocol: " << ".........." << coher_prot << endl;

  /* Print Statistics */

  cout << "*****************************************" << endl;
  cout << "Simulation results:" << endl;
  cout << "*****************************************" << endl;
  //cout << "CPU time (cycles):" << "........." << CPU_time << endl;
  //cout << "AMAT(cycles):" << ".............." << AMAT << endl;
  cout << "Overall miss rate" << ".........." << miss_rate_l2l1 << endl;
  cout << "CPU1 L1 miss rate:" << "........." << miss_rate_L1_CPU1 << endl;
  cout << "CPU2 L1 miss rate:" << "........." << miss_rate_L1_CPU2 << endl;
  cout << "Coherence Invalidation CPU1:" << "---" << LS_miss_hit_eviction_inv_count_for_L1_CPU1[5] << endl;
  cout << "Coherence Invalidation CPU2:" << "---" << LS_miss_hit_eviction_inv_count_for_L1_CPU2[5] << endl;
  //cout << "L2 miss rate:" << ".............." << miss_rate_L2 << endl;
  //cout << "Misses (L1):" << "..............." << total_misses_L1 << endl;
  //cout << "Hits (L1):" << "................." << total_hits_L1 << endl;
  //cout << "Misses (L2):" << "..............." << total_misses_L2 << endl;
  //cout << "Hits (L2):" << "................." << total_hits_L2 << endl;
  //cout << "Dirty evictions (L2):" << "......" << l2_stats_count[4] << endl;
  //cout << "Load misses:" << "..............." << LS_miss_hit_eviction_count[0] << endl;
  //cout << "Store misses:" << ".............." << LS_miss_hit_eviction_count[1] << endl;
  //cout << "Load hits:" << "................." << LS_miss_hit_eviction_count[2] << endl;
  //cout << "Store hits:" << "................" << LS_miss_hit_eviction_count[3] << endl;
  //cout << "Total memory accesses:" << "....." << accesses << endl;
  cout << "*****************************************" << endl;
  /*
  LS_miss_hit_eviction_count[0] = LOAD_MISS;
  LS_miss_hit_eviction_count[1] = STORE_MISS;
  LS_miss_hit_eviction_count[2] = LOAD_HIT;
  LS_miss_hit_eviction_count[3] = STORE_HIT;
  LS_miss_hit_eviction_count[4] = dirty_evictions;
  */

tf = clock();
double execution_time = (double(tf-ti)/CLOCKS_PER_SEC);
cout << "Execution time:" << "............" << execution_time << endl;
  return 0;
}



/*
 *
 * 
 * 
 * 
 * 
 *                    L1 CACHE FUNTIONS
 * 
 *      Se agregan todas las funciones de l1cache al final del main.cpp 
 *        para facilitar la compilacion del programa con pthreads.
 * 
 *  
 * 
 *  */




entry **cache_build(int idx_size,
                    int associativity,
                    string rp)
{

  //-------------------------------cache build------------------------------
  int ways;
  if (associativity <= 0)
  {
    ways = 1;
  }
  else
  {
    ways = associativity;
  }
  //creates a cache with "idx_size" elements, each idx has "ways" elements
  //idx size is the number of bits, so the real size would be:
  // 2^(idx_size)
  int number_of_indexes = pow(2, idx_size);

  entry **l1_cache = new entry *[number_of_indexes];
  //cout << "number_of_indexes: " << number_of_indexes << endl;
  //cout << "ways_in: " << ways << endl;

  for (int i = 0; i < number_of_indexes; i++)
  {
    l1_cache[i] = new entry[ways];
    //cout << "coco: " << i << endl;
  }

  //cout << "coco: " << ways << endl;

  for (int i = 0; i < number_of_indexes; i++)
  {
    for (int j = 0; j < ways; j++)
    {
      l1_cache[i][j].valid = 0;
      l1_cache[i][j].dirty = 0;
      l1_cache[i][j].tag = 0;

      l1_cache[i][j].coherence_protocol = INVALID;

      if (rp == "lru")
      {
        l1_cache[i][j].rp_value = 0;
        // cout << "rp_in: " << l1_cache[i][j].rp_value  << endl;
        // cout << i << endl;
      }
      else if (rp == "srrip")
      {
        l1_cache[i][j].rp_value = 3;
      }

      //SRRIP starts with RRPV = 3
      //LRU starts with RRPV = 0
    }
  }
  return l1_cache;
}

/*
 *
 * 
 *             FUNCION PARA THREADS
 *                FIELD_SIZE_GET
 * 
 * 
 *  */
void *field_size_get_cache_build(void *threadArgs)
{
  //    PTHREADS
  struct field_size_data *my_data;
  my_data = (struct field_size_data *)threadArgs;

  //check if user picked the correct replacement policy
  bool rp_check;
  if (my_data->rp == "lru" || my_data->rp == "srrip")
  {
    rp_check = true;
  }
  else
  {
    rp_check = false;
  }
  //check if cache_sze and block_size are correct sizes which come from a pow of 2
  bool cachesize_check = (my_data->cachesize_kb & (my_data->cachesize_kb - 1)) == 0;
  bool blocksize_check = (my_data->blocksize_bytes & (my_data->blocksize_bytes - 1)) == 0;

  //----------------------------------end check-------------------------------

  //-------------------------------getting sizes------------------------------
  int ways;
  int cache_true_size;
  if (my_data->associativity <= 0)
  {
    ways = 1;
    //get cache input to Bytes
    cache_true_size = my_data->cachesize_kb;
    //Example: if 4 kB .... then cache_true_size will be 4096 B
  }
  else
  {
    ways = my_data->associativity;
    //get cache input to Bytes
    cache_true_size = my_data->cachesize_kb * KB;
    //Example: if 4 kB .... then cache_true_size will be 4096 B
  }

  *my_data->offset_size = log2(my_data->blocksize_bytes);
  *my_data->idx_size = log2(cache_true_size / (my_data->blocksize_bytes * ways));
  *my_data->tag_size = ADDRSIZE - *my_data->idx_size - *my_data->offset_size;

  pthread_exit(NULL);
}

/*
 *
 * 
 * 
 *     ADDRESS TAG INDEX GET
 * 
 * 
 * 
 * 
 */

void *address_tag_idx_get(void *threadArgs)
{

  struct tag_idx_data *my_data;
  my_data = (struct tag_idx_data *)threadArgs;

  //adds 1's, then shift em left the proper size to create
  //the idx and tag masks
  int idx_mask = 0;
  int tag_mask = 0;
  //cout << "address00: " << address << endl;

  //Creates mask for index
  for (int i = 0; i < my_data->idx_size; i++)
  {
    idx_mask = idx_mask << 1;
    idx_mask = idx_mask + 1;
  }
  // cout << "idx_mask00: " << idx_mask << endl;

  idx_mask = idx_mask << my_data->offset_size;

  // cout << "idx_mask33: " << idx_mask << endl;

  //----------------------------------------------------------------------------
  //Creates mask for tag
  for (int i = 0; i < my_data->tag_size; i++)
  {
    tag_mask = tag_mask << 1;
    tag_mask = tag_mask + 1;
  }

  // cout << "tag_mask: " << tag_mask << endl;

  tag_mask = tag_mask << (my_data->offset_size + my_data->idx_size);

  // cout << "tag_mask333: " << tag_mask << endl;

  //----------------------------------------------------------------------------

  *my_data->idx = my_data->address & idx_mask;
  *my_data->idx = *my_data->idx >> my_data->offset_size;

  *my_data->tag = my_data->address >> (ADDRSIZE - my_data->tag_size);
  pthread_exit(NULL);
}

int lru_msi_mesi_prot(int number_of_entries,
                      int tag,
                      int associativity,
                      bool loadstore,
                      bool msi_mesi,
                      entry *cache_blocks,
                      entry *cache_blocks_CPU2,
                      operation_result *result,
                      int *LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                      int *LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                      bool debug)
{

  //LRU can be made with positions, storing each value to an indexed position,
  //but with RRPV, you need to set most recently used (MRU) element to the highest value
  //(associativity - 1) and the least recently used to 0.

  //LRU
  // Cache Hit:
  // (i) decrement all higher RRPVs
  // (ii) set RRPV of block to ‘associativity - 1’ (Rereference Prediction Values)

  // Cache Miss:
  // (i) search for ‘0’
  // (ii) decrement all higher RRPVs (Rereference Prediction Values)
  // (iii) replace block and set RRPV to ‘associativity - 1’

  //pruebas
  // cout << "wew: " <<  tag << endl;
  //-----------------------------------

  int ways_num = 0;
  if (debug == 1)
  {
    cout << "IN_LRU---number_of_entries: " << number_of_entries << endl;
    cout << "IN_LRU---tag: " << tag << endl;
    // cout << "IN_LRU---tag in cache: " << cache_blocks[number_of_entries-1].tag << endl;
    cout << "IN_LRU---associativity: " << associativity << endl;
    cout << "IN_LRU---loadstore: " << loadstore << endl;
    // cout << "IN_LRU---valid: " << cache_blocks[number_of_entries-1].valid << endl;
  }

  if (associativity <= 1)
  {

    ways_num = number_of_entries;
  }
  else
  {
    ways_num = associativity;
  }
  // if associativity is 1, the idx supplies the associativity number, so search from 0 to (idx -1)
  //cout<<"\nways_num: "<< ways_num <<endl;

  //------------------------------------HIT------------------------------------
  //search for tag in each idx way
  for (int way = 0; way < ways_num; way++)
  {
    if (cache_blocks[way].valid && tag == cache_blocks[way].tag)
    {

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = HIT_LOAD;
        //cache_blocks[way].dirty = false;
        if (msi_mesi == true && cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = EXCLUSIVE;
        }
        else if (cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = SHARED;
        }
        else
        {
          cache_blocks_CPU2[way].coherence_protocol = SHARED;
          cache_blocks[way].coherence_protocol = SHARED;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[2]++;
      }
      else
      {
        result->miss_hit = HIT_STORE;
        cache_blocks[way].dirty = true;
        cache_blocks[way].coherence_protocol = MODIFIED;
        if (cache_blocks_CPU2[way].coherence_protocol != INVALID)
        {
          LS_miss_hit_eviction_inv_count_for_L1_CPU2[5]++;
          cache_blocks_CPU2[way].coherence_protocol = INVALID;
          cache_blocks_CPU2[way].valid = 0;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[3]++;
      }

      //No evicted block
      result->dirty_eviction = false;
      result->evicted_address = 0;

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (i)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(ii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used

      return OK; //ends function
    }            // if hit
  }              //hit for

  //------------------------------------MISS------------------------------------

  for (int way = 0; way < ways_num; way++)
  {
    //(i)
    if (cache_blocks[way].rp_value == 0)
    {

      //if block is valid, it had an element, needs block eviction
      //if block is invalid, just replace it without eviction
      if (cache_blocks[way].valid == 1)
      {

        //check if block is dirty, for eviction and writeback
        if (cache_blocks[way].dirty == true)
        {
          result->dirty_eviction = true;
          LS_miss_hit_eviction_inv_count_for_L1_CPU1[4]++;
        }
        else
        {
          result->dirty_eviction = false;
        }
        result->evicted_address = cache_blocks[way].tag;
      }
      else
      {
        //there is no evicted address if there was no block (initial state)
        result->dirty_eviction = false;
        result->evicted_address = 0;
      } //valid_else

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = MISS_LOAD;
        cache_blocks[way].dirty = false;
        if (msi_mesi == true && cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = EXCLUSIVE;
        }
        else if (cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = SHARED;
        }
        else
        {
          cache_blocks_CPU2[way].coherence_protocol = SHARED;
          cache_blocks[way].coherence_protocol = SHARED;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[0]++;
      }
      else
      {
        result->miss_hit = MISS_STORE;
        cache_blocks[way].dirty = true;
        cache_blocks[way].coherence_protocol = MODIFIED;
        if (cache_blocks_CPU2[way].coherence_protocol != INVALID)
        {
          LS_miss_hit_eviction_inv_count_for_L1_CPU2[5]++;
          cache_blocks_CPU2[way].coherence_protocol = INVALID;
          cache_blocks_CPU2[way].valid = 0;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[1]++;
      }

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (ii)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(iii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used
      cache_blocks[way].valid = true;
      cache_blocks[way].tag = tag;

      return OK;
    }
  } // miss for

  //---------------------------------------END------------------------------------
} //end lru

int lru_L2_cache(int number_of_entries,
                 int tag,
                 int associativity,
                 bool loadstore,
                 entry *cache_blocks,
                 operation_result *result,
                 int *LS_miss_hit_eviction_count,
                 bool debug)
{

  //LRU can be made with positions, storing each value to an indexed position,
  //but with RRPV, you need to set most recently used (MRU) element to the highest value
  //(associativity - 1) and the least recently used to 0.

  //LRU
  // Cache Hit:
  // (i) decrement all higher RRPVs
  // (ii) set RRPV of block to ‘associativity - 1’ (Rereference Prediction Values)

  // Cache Miss:
  // (i) search for ‘0’
  // (ii) decrement all higher RRPVs (Rereference Prediction Values)
  // (iii) replace block and set RRPV to ‘associativity - 1’

  //pruebas
  // cout << "wew: " <<  tag << endl;
  //-----------------------------------

  int ways_num = 0;
  if (debug == 1)
  {
    cout << "IN_LRU---number_of_entries: " << number_of_entries << endl;
    cout << "IN_LRU---tag: " << tag << endl;
    // cout << "IN_LRU---tag in cache: " << cache_blocks[number_of_entries-1].tag << endl;
    cout << "IN_LRU---associativity: " << associativity << endl;
    cout << "IN_LRU---loadstore: " << loadstore << endl;
    // cout << "IN_LRU---valid: " << cache_blocks[number_of_entries-1].valid << endl;
  }

  if (associativity <= 1)
  {

    ways_num = number_of_entries;
  }
  else
  {
    ways_num = associativity;
  }
  // if associativity is 1, the idx supplies the associativity number, so search from 0 to (idx -1)
  //cout<<"\nways_num: "<< ways_num <<endl;
  //------------------------------------HIT------------------------------------
  //search for tag in each idx way
  for (int way = 0; way < ways_num; way++)
  {
    if (cache_blocks[way].valid && tag == cache_blocks[way].tag)
    {

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = HIT_LOAD;
        //cache_blocks[way].dirty = false;
        LS_miss_hit_eviction_count[2]++;
      }
      else
      {
        result->miss_hit = HIT_STORE;
        cache_blocks[way].dirty = true;
        LS_miss_hit_eviction_count[3]++;
      }

      //No evicted block
      result->dirty_eviction = false;
      result->evicted_address = 0;

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (i)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(ii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used

      return OK; //ends function
    }            // if hit
  }              //hit for

  //------------------------------------MISS------------------------------------

  for (int way = 0; way < ways_num; way++)
  {
    //(i)
    if (cache_blocks[way].rp_value == 0)
    {

      //if block is valid, it had an element, needs block eviction
      //if block is invalid, just replace it without eviction
      if (cache_blocks[way].valid == 1)
      {

        //check if block is dirty, for eviction and writeback
        if (cache_blocks[way].dirty == true)
        {
          result->dirty_eviction = true;
          LS_miss_hit_eviction_count[4]++;
        }
        else
        {
          result->dirty_eviction = false;
        }
        result->evicted_address = cache_blocks[way].tag;
      }
      else
      {
        //there is no evicted address if there was no block (initial state)
        result->dirty_eviction = false;
        result->evicted_address = 0;
      } //valid_else

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = MISS_LOAD;
        cache_blocks[way].dirty = false;
        LS_miss_hit_eviction_count[0]++;
      }
      else
      {
        result->miss_hit = MISS_STORE;
        cache_blocks[way].dirty = true;
        LS_miss_hit_eviction_count[1]++;
      }

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (ii)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(iii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used
      cache_blocks[way].valid = true;
      cache_blocks[way].tag = tag;

      return OK;
    }
  } // miss for

} //end lru

int lru_multilevel_cache(int idx_L1,
                         int idx_L2,
                         int tag_L1,
                         int tag_L2,

                         int associativity_L1,
                         int associativity_L2, //number of entries if it is 0

                         bool loadstore,
                         bool msi_mesi,

                         entry *cache_L1_CPU1_blocks,
                         entry *cache_L1_CPU2_blocks,
                         entry *cache_L2_blocks,
                         operation_result *operation_result_for_L1,
                         operation_result *operation_result_for_L2,

                         int *LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                         int *LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                         int *LS_miss_hit_eviction_count_for_L2,
                         bool debug)

{

  /*
  if hit in L1, stay in L1.
  if miss in L1, check L2, if miss in L2 writeback to main memory.
  if hit in L2, put that element into L1, evicted address from L1, goes away
  */
  int status;

  //if there's a miss, always save the element in L1
  status = lru_msi_mesi_prot(idx_L1,
                             tag_L1,
                             associativity_L1,
                             loadstore,
                             msi_mesi,
                             cache_L1_CPU1_blocks,
                             cache_L1_CPU2_blocks,
                             operation_result_for_L1,
                             LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                             LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                             debug);

  //go to L2 only if there's a miss in L1

  if (operation_result_for_L1->miss_hit == MISS_LOAD || operation_result_for_L1->miss_hit == MISS_STORE)
  {
    //cout << "\nwewe" << endl;

    status = lru_L2_cache(idx_L2,
                          tag_L2,
                          associativity_L2,
                          loadstore,
                          cache_L2_blocks,
                          operation_result_for_L2,
                          LS_miss_hit_eviction_count_for_L2,
                          debug);
  }
  //cout << "\nmisshit: " << operation_result_for_L2->miss_hit << endl;

  return OK;

} //end multilevel
