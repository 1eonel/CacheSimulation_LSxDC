# PTHREADS

Para solucionar rápidamente los problemas de compilación discutidos en la reunión, se optó por copiar todos los métodos y funciones del archivo L1cache.cpp, adentro del archivo main.cpp, de esta manera el programa compiló sin problemas al ejecutar:

```bash
g++ -pthreads main.cpp
```

Sin embargo, a pesar de la correcta compilación del código utilizando pthreads en las fuciones discutidas en la reunión (field_size_get y address_tag_idx_get para L1s y L2s), el programa en lugar de mostrar una mejora en el rendimiento, muestra un deterioro en el tiempo de ejecución.

Al ejecutar el programa sin incluir pthreads, este presenta un tiempo de ejecución de 10 segundos, mientras que al ejecutar la misma versión pero con las paralelizaciones, este presente un tiempo de ejecución de 408 segundos, esto probablemente por una mala técnica a la hora de elegir los bloques de códigos a paralelizar, ya que probablemente son funciones que no valen la pena optimizar de esta manera.

Al final del pdf de plan de pruebas se incluye un ejemplo de la ejecución del código.
