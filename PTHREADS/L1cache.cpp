/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include "L1cache.h"

#include <pthread.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

entry **cache_build(int idx_size,
                    int associativity,
                    string rp)
{

  //-------------------------------cache build------------------------------
  int ways;
  if (associativity <= 0)
  {
    ways = 1;
  }
  else
  {
    ways = associativity;
  }
  //creates a cache with "idx_size" elements, each idx has "ways" elements
  //idx size is the number of bits, so the real size would be:
  // 2^(idx_size)
  int number_of_indexes = pow(2, idx_size);

  entry **l1_cache = new entry *[number_of_indexes];
  //cout << "number_of_indexes: " << number_of_indexes << endl;
  //cout << "ways_in: " << ways << endl;

  for (int i = 0; i < number_of_indexes; i++)
  {
    l1_cache[i] = new entry[ways];
    //cout << "coco: " << i << endl;
  }

  //cout << "coco: " << ways << endl;

  for (int i = 0; i < number_of_indexes; i++)
  {
    for (int j = 0; j < ways; j++)
    {
      l1_cache[i][j].valid = 0;
      l1_cache[i][j].dirty = 0;
      l1_cache[i][j].tag = 0;

      l1_cache[i][j].coherence_protocol = INVALID;

      if (rp == "lru")
      {
        l1_cache[i][j].rp_value = 0;
        // cout << "rp_in: " << l1_cache[i][j].rp_value  << endl;
        // cout << i << endl;
      }
      else if (rp == "srrip")
      {
        l1_cache[i][j].rp_value = 3;
      }

      //SRRIP starts with RRPV = 3
      //LRU starts with RRPV = 0
    }
  }
  return l1_cache;
}

/*
 *
 * 
 *             FUNCION PARA THREADS
 *                FIELD_SIZE_GET
 * 
 * 
 *  */
void *field_size_get_cache_build(void *threadArgs)
{
  //    PTHREADS
  struct field_size_data *my_data;
  my_data = (struct field_size_data *)threadArgs;

  //check if user picked the correct replacement policy
  bool rp_check;
  if (my_data->rp == "lru" || my_data->rp == "srrip")
  {
    rp_check = true;
  }
  else
  {
    rp_check = false;
  }
  //check if cache_sze and block_size are correct sizes which come from a pow of 2
  bool cachesize_check = (my_data->cachesize_kb & (my_data->cachesize_kb - 1)) == 0;
  bool blocksize_check = (my_data->blocksize_bytes & (my_data->blocksize_bytes - 1)) == 0;

  //----------------------------------end check-------------------------------

  //-------------------------------getting sizes------------------------------
  int ways;
  int cache_true_size;
  if (my_data->associativity <= 0)
  {
    ways = 1;
    //get cache input to Bytes
    cache_true_size = my_data->cachesize_kb;
    //Example: if 4 kB .... then cache_true_size will be 4096 B
  }
  else
  {
    ways = my_data->associativity;
    //get cache input to Bytes
    cache_true_size = my_data->cachesize_kb * KB;
    //Example: if 4 kB .... then cache_true_size will be 4096 B
  }

  *my_data->offset_size = log2(my_data->blocksize_bytes);
  *my_data->idx_size = log2(cache_true_size / (my_data->blocksize_bytes * ways));
  *my_data->tag_size = ADDRSIZE - *my_data->idx_size - *my_data->offset_size;

  pthread_exit(NULL);
}

/*
 *
 * 
 * 
 *     ADDRESS TAG INDEX GET
 * 
 * 
 * 
 * 
 */

void *address_tag_idx_get(void *threadArgs)
{

  struct tag_idx_data *my_data;
  my_data = (struct tag_idx_data *)threadArgs;

  //adds 1's, then shift em left the proper size to create
  //the idx and tag masks
  int idx_mask = 0;
  int tag_mask = 0;
  //cout << "address00: " << address << endl;

  //Creates mask for index
  for (int i = 0; i < my_data->idx_size; i++)
  {
    idx_mask = idx_mask << 1;
    idx_mask = idx_mask + 1;
  }
  // cout << "idx_mask00: " << idx_mask << endl;

  idx_mask = idx_mask << my_data->offset_size;

  // cout << "idx_mask33: " << idx_mask << endl;

  //----------------------------------------------------------------------------
  //Creates mask for tag
  for (int i = 0; i < my_data->tag_size; i++)
  {
    tag_mask = tag_mask << 1;
    tag_mask = tag_mask + 1;
  }

  // cout << "tag_mask: " << tag_mask << endl;

  tag_mask = tag_mask << (my_data->offset_size + my_data->idx_size);

  // cout << "tag_mask333: " << tag_mask << endl;

  //----------------------------------------------------------------------------

  *my_data->idx = my_data->address & idx_mask;
  *my_data->idx = *my_data->idx >> my_data->offset_size;

  *my_data->tag = my_data->address >> (ADDRSIZE - my_data->tag_size);
  pthread_exit(NULL);
}

int lru_msi_mesi_prot(int number_of_entries,
                      int tag,
                      int associativity,
                      bool loadstore,
                      bool msi_mesi,
                      entry *cache_blocks,
                      entry *cache_blocks_CPU2,
                      operation_result *result,
                      int *LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                      int *LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                      bool debug)
{

  //LRU can be made with positions, storing each value to an indexed position,
  //but with RRPV, you need to set most recently used (MRU) element to the highest value
  //(associativity - 1) and the least recently used to 0.

  //LRU
  // Cache Hit:
  // (i) decrement all higher RRPVs
  // (ii) set RRPV of block to ‘associativity - 1’ (Rereference Prediction Values)

  // Cache Miss:
  // (i) search for ‘0’
  // (ii) decrement all higher RRPVs (Rereference Prediction Values)
  // (iii) replace block and set RRPV to ‘associativity - 1’

  //pruebas
  // cout << "wew: " <<  tag << endl;
  //-----------------------------------

  int ways_num = 0;
  if (debug == 1)
  {
    cout << "IN_LRU---number_of_entries: " << number_of_entries << endl;
    cout << "IN_LRU---tag: " << tag << endl;
    // cout << "IN_LRU---tag in cache: " << cache_blocks[number_of_entries-1].tag << endl;
    cout << "IN_LRU---associativity: " << associativity << endl;
    cout << "IN_LRU---loadstore: " << loadstore << endl;
    // cout << "IN_LRU---valid: " << cache_blocks[number_of_entries-1].valid << endl;
  }

  if (associativity <= 1)
  {

    ways_num = number_of_entries;
  }
  else
  {
    ways_num = associativity;
  }
  // if associativity is 1, the idx supplies the associativity number, so search from 0 to (idx -1)
  //cout<<"\nways_num: "<< ways_num <<endl;

  //------------------------------------HIT------------------------------------
  //search for tag in each idx way
  for (int way = 0; way < ways_num; way++)
  {
    if (cache_blocks[way].valid && tag == cache_blocks[way].tag)
    {

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = HIT_LOAD;
        //cache_blocks[way].dirty = false;
        if (msi_mesi == true && cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = EXCLUSIVE;
        }
        else if (cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = SHARED;
        }
        else
        {
          cache_blocks_CPU2[way].coherence_protocol = SHARED;
          cache_blocks[way].coherence_protocol = SHARED;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[2]++;
      }
      else
      {
        result->miss_hit = HIT_STORE;
        cache_blocks[way].dirty = true;
        cache_blocks[way].coherence_protocol = MODIFIED;
        if (cache_blocks_CPU2[way].coherence_protocol != INVALID)
        {
          LS_miss_hit_eviction_inv_count_for_L1_CPU2[5]++;
          cache_blocks_CPU2[way].coherence_protocol = INVALID;
          cache_blocks_CPU2[way].valid = 0;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[3]++;
      }

      //No evicted block
      result->dirty_eviction = false;
      result->evicted_address = 0;

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (i)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(ii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used

      return OK; //ends function
    }            // if hit
  }              //hit for

  //------------------------------------MISS------------------------------------

  for (int way = 0; way < ways_num; way++)
  {
    //(i)
    if (cache_blocks[way].rp_value == 0)
    {

      //if block is valid, it had an element, needs block eviction
      //if block is invalid, just replace it without eviction
      if (cache_blocks[way].valid == 1)
      {

        //check if block is dirty, for eviction and writeback
        if (cache_blocks[way].dirty == true)
        {
          result->dirty_eviction = true;
          LS_miss_hit_eviction_inv_count_for_L1_CPU1[4]++;
        }
        else
        {
          result->dirty_eviction = false;
        }
        result->evicted_address = cache_blocks[way].tag;
      }
      else
      {
        //there is no evicted address if there was no block (initial state)
        result->dirty_eviction = false;
        result->evicted_address = 0;
      } //valid_else

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = MISS_LOAD;
        cache_blocks[way].dirty = false;
        if (msi_mesi == true && cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = EXCLUSIVE;
        }
        else if (cache_blocks_CPU2[way].coherence_protocol == INVALID)
        {
          cache_blocks[way].coherence_protocol = SHARED;
        }
        else
        {
          cache_blocks_CPU2[way].coherence_protocol = SHARED;
          cache_blocks[way].coherence_protocol = SHARED;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[0]++;
      }
      else
      {
        result->miss_hit = MISS_STORE;
        cache_blocks[way].dirty = true;
        cache_blocks[way].coherence_protocol = MODIFIED;
        if (cache_blocks_CPU2[way].coherence_protocol != INVALID)
        {
          LS_miss_hit_eviction_inv_count_for_L1_CPU2[5]++;
          cache_blocks_CPU2[way].coherence_protocol = INVALID;
          cache_blocks_CPU2[way].valid = 0;
        }
        LS_miss_hit_eviction_inv_count_for_L1_CPU1[1]++;
      }

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (ii)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(iii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used
      cache_blocks[way].valid = true;
      cache_blocks[way].tag = tag;

      return OK;
    }
  } // miss for

  //---------------------------------------END------------------------------------
} //end lru

int lru_L2_cache(int number_of_entries,
                 int tag,
                 int associativity,
                 bool loadstore,
                 entry *cache_blocks,
                 operation_result *result,
                 int *LS_miss_hit_eviction_count,
                 bool debug)
{

  //LRU can be made with positions, storing each value to an indexed position,
  //but with RRPV, you need to set most recently used (MRU) element to the highest value
  //(associativity - 1) and the least recently used to 0.

  //LRU
  // Cache Hit:
  // (i) decrement all higher RRPVs
  // (ii) set RRPV of block to ‘associativity - 1’ (Rereference Prediction Values)

  // Cache Miss:
  // (i) search for ‘0’
  // (ii) decrement all higher RRPVs (Rereference Prediction Values)
  // (iii) replace block and set RRPV to ‘associativity - 1’

  //pruebas
  // cout << "wew: " <<  tag << endl;
  //-----------------------------------

  int ways_num = 0;
  if (debug == 1)
  {
    cout << "IN_LRU---number_of_entries: " << number_of_entries << endl;
    cout << "IN_LRU---tag: " << tag << endl;
    // cout << "IN_LRU---tag in cache: " << cache_blocks[number_of_entries-1].tag << endl;
    cout << "IN_LRU---associativity: " << associativity << endl;
    cout << "IN_LRU---loadstore: " << loadstore << endl;
    // cout << "IN_LRU---valid: " << cache_blocks[number_of_entries-1].valid << endl;
  }

  if (associativity <= 1)
  {

    ways_num = number_of_entries;
  }
  else
  {
    ways_num = associativity;
  }
  // if associativity is 1, the idx supplies the associativity number, so search from 0 to (idx -1)
  //cout<<"\nways_num: "<< ways_num <<endl;
  //------------------------------------HIT------------------------------------
  //search for tag in each idx way
  for (int way = 0; way < ways_num; way++)
  {
    if (cache_blocks[way].valid && tag == cache_blocks[way].tag)
    {

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = HIT_LOAD;
        //cache_blocks[way].dirty = false;
        LS_miss_hit_eviction_count[2]++;
      }
      else
      {
        result->miss_hit = HIT_STORE;
        cache_blocks[way].dirty = true;
        LS_miss_hit_eviction_count[3]++;
      }

      //No evicted block
      result->dirty_eviction = false;
      result->evicted_address = 0;

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (i)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(ii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used

      return OK; //ends function
    }            // if hit
  }              //hit for

  //------------------------------------MISS------------------------------------

  for (int way = 0; way < ways_num; way++)
  {
    //(i)
    if (cache_blocks[way].rp_value == 0)
    {

      //if block is valid, it had an element, needs block eviction
      //if block is invalid, just replace it without eviction
      if (cache_blocks[way].valid == 1)
      {

        //check if block is dirty, for eviction and writeback
        if (cache_blocks[way].dirty == true)
        {
          result->dirty_eviction = true;
          LS_miss_hit_eviction_count[4]++;
        }
        else
        {
          result->dirty_eviction = false;
        }
        result->evicted_address = cache_blocks[way].tag;
      }
      else
      {
        //there is no evicted address if there was no block (initial state)
        result->dirty_eviction = false;
        result->evicted_address = 0;
      } //valid_else

      //if loadstore == 0; LOAD
      //else; is STORE
      if (loadstore == false)
      {
        result->miss_hit = MISS_LOAD;
        cache_blocks[way].dirty = false;
        LS_miss_hit_eviction_count[0]++;
      }
      else
      {
        result->miss_hit = MISS_STORE;
        cache_blocks[way].dirty = true;
        LS_miss_hit_eviction_count[1]++;
      }

      //LRU replacement policy
      if (cache_blocks[way].rp_value < ways_num - 1)
      {
        for (int i = 0; i < ways_num; i++)
        {
          // (ii)
          if (cache_blocks[i].rp_value > cache_blocks[way].rp_value)
          {
            cache_blocks[i].rp_value--;
          }
        }
      }
      //(iii)
      cache_blocks[way].rp_value = ways_num - 1; // becomes most recently used
      cache_blocks[way].valid = true;
      cache_blocks[way].tag = tag;

      return OK;
    }
  } // miss for

} //end lru

int lru_multilevel_cache(int idx_L1,
                         int idx_L2,
                         int tag_L1,
                         int tag_L2,

                         int associativity_L1,
                         int associativity_L2, //number of entries if it is 0

                         bool loadstore,
                         bool msi_mesi,

                         entry *cache_L1_CPU1_blocks,
                         entry *cache_L1_CPU2_blocks,
                         entry *cache_L2_blocks,
                         operation_result *operation_result_for_L1,
                         operation_result *operation_result_for_L2,

                         int *LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                         int *LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                         int *LS_miss_hit_eviction_count_for_L2,
                         bool debug)

{

  /*
  if hit in L1, stay in L1.
  if miss in L1, check L2, if miss in L2 writeback to main memory.
  if hit in L2, put that element into L1, evicted address from L1, goes away
  */
  int status;

  //if there's a miss, always save the element in L1
  status = lru_msi_mesi_prot(idx_L1,
                             tag_L1,
                             associativity_L1,
                             loadstore,
                             msi_mesi,
                             cache_L1_CPU1_blocks,
                             cache_L1_CPU2_blocks,
                             operation_result_for_L1,
                             LS_miss_hit_eviction_inv_count_for_L1_CPU1,
                             LS_miss_hit_eviction_inv_count_for_L1_CPU2,
                             debug);

  //go to L2 only if there's a miss in L1

  if (operation_result_for_L1->miss_hit == MISS_LOAD || operation_result_for_L1->miss_hit == MISS_STORE)
  {
    //cout << "\nwewe" << endl;

    status = lru_L2_cache(idx_L2,
                          tag_L2,
                          associativity_L2,
                          loadstore,
                          cache_L2_blocks,
                          operation_result_for_L2,
                          LS_miss_hit_eviction_count_for_L2,
                          debug);
  }
  //cout << "\nmisshit: " << operation_result_for_L2->miss_hit << endl;

  return OK;

} //end multilevel
